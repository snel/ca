\documentclass{article}
\usepackage[a4paper, margin=1.2in]{geometry}

% Math packages
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{float}
\usepackage[bookmarks=false, breaklinks=true]{hyperref}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{pgfplots}
\usepackage{caption}

\graphicspath{{../experiment1/}{../migration_experiments/}}

% Illustration packages
%\usepackage{tikz}
%\usetikzlibrary{positioning,shapes,arrows,automata}

% Custom math definitions

\newcommand{\Set}[2]{\{#1\ :\ #2\}}
\newcommand{\Or}{\ |\ }
\newcommand{\set}[1] {%
	\mathbb{#1}%
}
\renewcommand{\vec}[1] {%
	\mathbf{#1}%
}
\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}

\newcommand{\savefootnote}[2]{\footnote{\label{#1}#2}}
\newcommand{\repeatfootnote}[1]{\textsuperscript{\ref{#1}}}

\newcommand{\img}[1]{%
\includegraphics[width=1.0\textwidth]{#1}
}
 
\newcommand{\graph}[1]{%
\begin{tikzpicture}
\begin{axis}[
    %width=\textwidth,
    height=1\textwidth,
    %grid=both,
    xmin=0,
    xmax=75,
    xticklabel style={/pgf/number format/1000 sep=, rotate=45},
    xlabel={Period},
    ymin=0,
    ymax=100,
    ylabel={Cooperators (\%)},
    compat=newest
]

\addplot table [x=Period, y=Cooperators, col sep=comma, mark=none,smooth] {../experiment1/#1.dat};
\end{axis}
\end{tikzpicture}
}

\newcommand{\figone}[2]{%
\begin{subfigure}{.4\textwidth}
  \centering
  \includegraphics[height=.9\textwidth]{#1}
  \caption{#2}
  \label{fig:#1_a}
\end{subfigure}%
\quad
\begin{subfigure}{.4\textwidth}
  \centering
  \graph{#1}
  \label{fig:#1_b}
\end{subfigure}
}

\begin{document}
\title{Modelling Social Dynamics by Cellular Automata\\
CS 711 group project report}

\author{
  S. E. A. Nel, \texttt{17903262}
  \and
  R. C. du Toit, \texttt{17816467}
  \and
  D. D. Malan, \texttt{17524695}
  \and
  K. R. De Klerk, \texttt{17861004}
}

\date{08/05/2016}

\maketitle
\section{Introduction}
This report describes efforts to confirm results set out in \cite{Hegselmann},
which set out to show how cellular automata can be used to model social
dynamics.
Every cell in an automaton is used to represent an individual in a society.
Hegselmann investigated two examples,
the first being \textit{N-person Prisoner dilemmas} using a checkerboard model
and the second, \textit{migration by evolution of support networks}.
We proceed by formalizing the experiments set out in Hegselmann's paper and
subsequently present the results of our experiments.
Finally, we analyse the results and attempt to explain the difficulties 
encountered.

\section{Overview}
In each experimental model that follows we define a 2-dimensional toroidal
lattice $L^t$, containing cell states $L^t(\vec{x}_i) = s_i^t$ that vary
over time.
The superscript $t$ is omitted whenever it is clear from the context.

Cellular Automaton (CA) neighbourhoods are often described as sets of cell
states, but instead we define the neighbourhood $\eta_i$ as the set of
locations
$\{\vec{x}_{j_0}, \vec{x}_{j_1}, \cdots ,\vec{x}_{j_{n-1}}\}$
nearby the center cell $C_i$ at location $\vec{x}_i$.
Moore neighbourhoods are described in terms of their half-width $r$, so that
they contain $(2r+1)^2$ locations.
In this report, von Neumann neighbourhoods are assumed to consist of 5
locations.
Unless otherwise specified, a cell forms part of its own neighbourhood.
Since the lattice is toroidal, all neighbourhoods are periodic.

Our experimental models deviate from standard cellular automata in a number of
ways.
Here are some notable characteristics of these models.
\begin{enumerate}
  \item
  In the first model, cells are non-uniform in the sense that only a subset
  of cells are updated during a time step.
  This is referred to as a period, but in fact corresponds to a single time
  step.
  In addition, the cells are updated in a random sequence.
  An update may depend on the outcome of previous updates in the sequence.
  Since the updates during a time step are not independent, this rule is not
  parallelizable.

  \item
  In the second model, agents may choose to migrate.
  This means that two cells exchange values.
  Since the possible destinations for two neighbouring non-empty cells may
  intersect, changes to the lattice during a time step are not independent
  and so this rule is not parallelizable.

  \item
  For a given starting configuration and set of parameters, the outcome of an
  experiment is not deterministic.
  In the first model, this is due to the random selection of cells in each
  period.
  In the second model, this is because cells are updated in a random sequence,
  migration options appear randomly and cells become needy according
  to some probability.
\end{enumerate}

\newpage
\section{Experimental model 1, fixed-site lattice}
We present a model consisting of a number of agents that are divided into two
groups, cooperators and defectors.
The agents are rational and know that the others are rational too.
This means that not only does each agent choose the option that profits them
the most as an individual, they make this decision knowing that the other
agents will do the same.

For each discrete time step, each agent chooses which group to belong to and
gains some reward depending on their choice.
This reward, in turn, depends on how many others are cooperating\footnote{
Any profits account only for the number of cooperating others in the agent's
neighbourhood.}.
The reward for defecting is greater than the reward for cooperating, as long
as enough others are cooperating.
The profit gained from defecting (the `temptation') decreases as the number
of defectors increases, so that if everyone defects, each individual profits
less than if everyone just worked together\footnote{
This is a multiplayer generalization of the prisoner's dilemma (PD),
referred to as ``the tragedy of the commons.''\cite{Stanford}}.
We now make this model precise.

\subsection{Lattice structure}\label{sec:model1lattice}
Let $\Sigma = \Set{\vec{s}}{\vec{s} \in (\{0,1\}, \set{R}, \set{R})}$ be the
set of states.
Consider the toroidal lattice $L^t(\vec{x}): 50 \times 50 \rightarrow \Sigma$.
Let $\vec{s}_i^t = L^t(\vec{x}_i)$ denote the state%
\savefootnote{imprecise}{A precise definition is not given in our source
paper\cite{Hegselmann}.}
of cell $C_i$ at time $t$ such that
\begin{enumerate}
  \item
  $(\vec{s}_i^t)_0 \in \{0,1\}$ denotes a defective (0) or cooperative (1)
  strategy during period $t$.

  \item
  $(\vec{s}_i^t)_1 \in \set{R}$ and $(\vec{s}_i^t)_2 \in \set{R}$ denote
  accounts used to book payoffs from cooperative and defective periods,
  respectively. This includes period $t$.
\end{enumerate}

\subsection{Payoff formulae}
Each agent $C_i$ is assigned two accounts, $(\vec{s}_i)_1$ and $(\vec{s}_i)_2$.
These accounts are credited according to so-called \emph{payoff formulae}
based on the weighted sum of cooperating others.
The weighting decreases as a function of Euclidean distance.

Let $\eta_i$ be a neighbourhood around $\vec{x}_i$. By definition,
$\vec{x}_i \in \eta_i$.

So, if $\vec{x}_i$ is the position of cell $C_i$ and $\vec{x}_j$ is the
position of its neighbour $C_j$, then for some $k \in \set{R}$, the
\emph{weighted sum} of distances is\footnote{
We exclude the term where $i = j$, because the distance from $\vec{x}_i$ to
itself is always 0.
Thus $d^k = || \vec{x}_i - \vec{x}_i ||^k = 0^k$ becomes
undefined when $k < 0$.
If $k=0$, then $d^k=0^0$ which is often defined to be 1, as we do here.
But if $k<0,$ then $d^k=\frac{1}{0^{-k}}=\frac{1}{0}$.}
\begin{equation}\label{model1w}
  w_i(\eta) = \sum_{\forall \vec{x}_j \in \eta \setminus \{\vec{x}_i\}}
  {|| \vec{x}_i - \vec{x}_j ||^k} \quad (\mbox{where }\eta \subseteq \eta_i.)
\end{equation}

Denote the positions of \emph{cooperating agents} around $\vec{x}_i$ as
$\widetilde \eta_i^t = \Set{\vec{x}_j \in \eta_i}{(\vec{s}_j^t)_0 = 1}$.
Then
\begin{equation}\label{model1q}
  \gamma_i^t = \frac{w_i(\widetilde \eta_i^t)}{w_i(\eta_i)}
\end{equation}
is the \emph{relative frequency}\footnote{
  \emph{Relative frequency:}
  How often something happens divided by all outcomes.
  For example, if your team has won 9 of 12 games, the relative frequency is
  $\frac{9}{12} = 0.75$.
}\repeatfootnote{imprecise}
of others cooperating.
Equations \ref{model1w} and \ref{model1q} were not given in \cite{Hegselmann}.
Also, the relationships between $\mbox{def}_i$, $\mbox{coop}_i$, $P_D$, $P_C$
and relative frequency were not clearly stated in that paper.
We assume that $\mbox{def}_i^t = P_D(\gamma_i^t)$ and
$\mbox{coop}_i^t = P_C(\gamma_i^t)$.
In \cite{Hegselmann}, these two functions are defined to be the same, except
for a constant difference $\beta=1$.
However, as we shall see later, this may need to be set significantly lower.
Thus the payoff formulae are
\begin{equation}\label{model1defcoop}
  \begin{array}{rll}
    \mbox{def}_i^t &= P_D(\gamma_i^t) &= 0.1 \gamma_i^t + \beta \\
    \mbox{coop}_i^t &= P_C(\gamma_i^t) &= 0.1 \gamma_i^t.
  \end{array}
\end{equation}

\subsection{Transition rule}
Here we derive the transition rule to be applied to each cell $C_i$ at
$\vec{x}_i$.
None of the equations in this subsection were given in \cite{Hegselmann}.
Let
\begin{equation}\label{model1h}
  h^t(\vec{x}_i) =
  \left\{
    \begin{array}{ll}
      1 & \mbox{if } g_2^t(\vec{x}_i) \geq g_1^t(\vec{x}_i) \\
      0 & \mbox{otherwise.}
    \end{array}
  \right.
  \quad \left( \mbox{where }
    g_p^t(\vec{x}_i) = \max_{\forall \vec{x}_j \in \eta_i} {(\vec{s}^t_j)_p}
  \right)
\end{equation}
Thus $h^t(\vec{x}_i)=1$ if the maximum COOP account in the neighbourhood of
$\vec{x}_i$ is greater or equal to the maximum DEF account, and 0 otherwise.

Define the transition rule\repeatfootnote{imprecise} as
\begin{equation}\label{model1phi}
  \vec{s}_i^{t+1} = \phi(\eta_i^t) =
  \left\{
    \begin{array}{ll}
      \left(
        h^t(\vec{x}_i),\
        (\vec{s}_i^t)_1,\
        \alpha (\vec{s}_i^t)_2  + \mbox{coop}_i^t
      \right)
      & \mbox{if } h^t(\vec{x}_i) = 1 \\
      \left(
        h^t(\vec{x}_i),\
        \alpha (\vec{s}_i^t)_1 + \mbox{def}_i^t,\
        (\vec{s}_i^t)_2
      \right)
      & \mbox{if } h^t(\vec{x}_i) = 0. \\
    \end{array}
  \right.
\end{equation}

\subsection{Experiments}
Our experiments investigate the following:
\begin{enumerate}
  \item What happens when the distance to other agents is a contributing factor
  to varying degrees?

  \item How does the choice of neighbourhood affect the outcome?
\end{enumerate}
In particular, we are interested in observing:
\begin{enumerate}
  \item an initial breakdown in cooperation, followed by a gradual increase;
  \item clustering patterns that develop over time; and
  \item convergence to an equilibrium.
\end{enumerate}
Three experiments are described, based on this model.
For each experiment, we do the following:\repeatfootnote{imprecise}
\begin{enumerate}
  \item
  At the start of each experiment, precisely half of the cells (1250 of 2500) are
  randomly selected and set to $(1, 0, 0)$.
  The others are set to $(0, 0, 0)$.

  \item
  1250 cells are randomly selected (from a uniform distribution) and rule
  $\phi(\eta_i^t)$ applied to each of them.
  That is, $s_i^{t+1} = \phi(\eta_i^t)$ for each selected cell $C_i$.
  This is called a \emph{period}.

  \item
  Repeat the second step 75 times.
\end{enumerate}

\begin{figure}[tb]
  \centering
  \figone{1ai}{$k = 0, \alpha = 0, \beta=1$}
  \figone{1aii}{$k = 0, \alpha = 0, \beta=0.001$}
  \figone{1aiii}{$k = 0, \alpha = 0.15, \beta=0.001$}
 
  \caption{Experiment 1(a), von Neumann neighbourhood ($r=1$).\\
  Small clusters of cooperators appear.\\
  In \ref{fig:1aiii_a}, the number of cooperators decreases and 
  remains constant at 45\%.\\
  In \cite{Hegselmann}, the number of cooperators decreases
  slightly, then increases slightly and remains constant at 50\%.
  }
  \label{fig:prisoner1a}
\end{figure}

\begin{figure}[tb]
  \centering
  \figone{1bi}{$k=-1, \alpha = 0.7, \beta=1$}
  \figone{1bii}{$k=-1, \alpha = 0.7, \beta=0.001$}
  \figone{1biii}{$k=-1, \alpha = 0.25, \beta=0.001$}
 
  \caption{Experiment 1(b), $3 \times 3$ Moore neighbourhood ($r=1$).\\
  Small cracks of defectors appear in a lattice otherwise occupied by
  cooperators.\\
  In \ref{fig:1biii_a}, the number of cooperators decreases slightly, then 
  increases and stays constant at 80\%.\\
  In \cite{Hegselmann}, the number of cooperators decreases slightly, then
  increases and stays constant at 80\%.
  }
  \label{fig:prisoner1b}
\end{figure}

\begin{figure}[tb]
  \centering
  \figone{1ci}{$k=-2, \alpha = 0.5, \beta=1$}
  \figone{1cii}{$k=-2, \alpha = 0.5, \beta=0.001$}
  \figone{1ciii}{$k=-2, \alpha = 0.01, \beta=0.001$}
 
  \caption{Experiment 1.c, $7 \times 7$ Moore neighbourhood ($r=3$).\\
  Large clusters of cooperators appear within mostly continuous areas occupied
  by defectors.\\
  In \ref{fig:1ciii_a}, the number of cooperators decreases and oscillates, 
  then stabilizes at 40\%.\\
  In \cite{Hegselmann}, the number of cooperators decreases almost to 0, then 
  slowly rises to 50\%.
  }
  \label{fig:prisoner1c}
\end{figure}

Results of these experiments are shown in figures \ref{fig:prisoner1a}, 
\ref{fig:prisoner1b} and \ref{fig:prisoner1c}.
The images on the left depict each lattice after 75 periods.
On the right is a plot of the ratio of cooperators over time.

\subsection{Analysis}
\subsubsection{Point of measurement}
In the figures we assume that the data points are measured at the end of each
period.
If accounts are measured at the start of each period, they are 0 during the
first period, and we would observe a spike in cooperation.
This is because if account values are equal, cooperation is the preferred
strategy (eq. \ref{model1h}).
This suggests that the accounts should be measured at the end of each period
(this was not obvious from \cite{Hegselmann}).
However, doing so causes 70\% of the population to cooperate by the end of the
first period. Contrast this with the initial 50\% that were set to
cooperate, and the 50\% initial value displayed in Hegselmann's graphs.

\subsubsection{Initial breakdown of cooperation}
The initial level of cooperation breaks down after after the first period.
Hegselmann describes this as a meaningful phenomenon, but we believe it is not.
Half of the cells are randomly selected during each period.
After the first period only half of the accounts have been updated,
but by the second period it is likely that all accounts have been updated
at least once (with probability $0.75$).
Thus, during the first few periods some accounts are still 0 simply
because they have not been updated yet.
As described in the previous paragraph, cooperation is the preferred strategy
when accounts are equal, and in particular, when they are 0.
Therefore the initial breakdown is directly caused by accounts being initialized
to a constant value (0).

\subsubsection{Difficulty reproducing results}
When using the parameter values as given by Hegselmann, all cells
consistently defect within only 10 periods.
We found that changing the payoff function was the only way to prevent this.
We introduced a new parameter, $\beta$ which, when set sufficiently small,
caused our experiments to resemble those from our source paper.

If the payoff function is changed and the values of $\alpha$ remain the same
as suggested by Hegselmann, the graphs all tend to an equilibrium.
However, this equilibrium is achieved a lot faster and at a higher percentage
than in his paper.
In the second part of each experiment, almost all cells become cooperators.
If we reduce the value of $\alpha$, the value of the equilibrium is lowered as
well as the rate at which this equilibrium is reached.

\subsubsection{Deviations}
After the the adjustments to $\alpha$ and $\beta$, we found that our lattices
produced certain patterns comparable to \cite{Hegselmann}.
Our graphs of cooperators over time deviate somewhat from those in the
paper, but there is a rough correspondance between our work and Hegselmann's.
The differences are probably due to a lack of clarity surrounding the starting
conditions and the precise manner that the payoffs are accrued.

%\newpage
\section{Experimental model 2, migration}
Once again, this model consists of a number of rational agents operating in
discrete time steps.
But unlike our first model, cooperating agents are not described as belonging
to one of two groups.

Now agents are assigned to various risk classes that are subject to becoming
needy and can form support networks to help each other when this happens.
We do not keep track of accounts for each agent.
Instead, agents are allowed to migrate to seek more favourable partners.

\subsection{Lattice structure}
Let $\Sigma = \{0,1,2,3,4,5,6,7,8,9\}$ be the set of states.
Consider the toroidal lattice
$L^t(\vec{x}_i): 21 \times 21 \rightarrow \Sigma$.
Let $s_i^t = L^t(\vec{x}_i)$ denote the state of cell $C_i$ at time $t$.
A value of 0 indicates an empty cell.
Other values correspond to 9 risk classes that become needy with probability
$p = 0.1, 0.2, \cdots, 0.9$.

\subsection{Neighbourhoods}
Consider a cell $C_i$.
Let $\eta_i^I$, $\eta_i^M$ and $\eta_i^A$
denote the information, migration and interaction windows of $C_i$,
respectively.
$C_i$ is able to see the risk classes of cells in the information window.
The migration window contains all of the empty cells to which cell $C_i$
may migrate to when it gets the opportunity.
Finally, the interaction window contains the cells with which cell $C_i$ can
form support relationships.

Both the information and migration windows are Moore neighbourhoods where
$\eta_i^I$ has $r=6$, and $\eta_i^M$ has $r=5$.
The interaction window $\eta_i^A$ is a von Neumann neighbourhood with $r=1$.
None of these neighbourhoods include $\vec{x}_i$:
\begin{enumerate}
  \item
  It makes no difference whether or not $\vec{x}_i \in \eta_i^I$, because it
  only appears in eq. \ref{model2p},
  where $p_i^*$ is defined as the probability of another cell supporting $C_i$.
  Thus $\vec{x}_i \notin \eta_i^I$.

  \item
  $\vec{x}_i$ cannot be in $\eta_i^M$, otherwise the location an actor
  migrates to could be the same as where it came from.
  Thus $\vec{x}_i \notin \eta_i^M$.

  \item
  $\vec{x}_i$ cannot be in $\eta_i^A$, because $C_i \sim C_j$ if and only if
  $i \neq j$ (eq. \ref{supportrelation}). Thus $\vec{x}_i \notin \eta_i^A$.
\end{enumerate}

\subsection{Support relationships}
Let $p_i = 0.1 s_i$ denote the probability that $C_i$ becomes needy and
set $S=1, D=5, M=6, H=7$ as the payoffs for being saved, drowning, moving on
and helping, respectively.

The PD-condition (eq. 5 in \cite{Hegselmann}) describes when mutual support is
profitable for both agents.
Stated differently, it describes a condition under which the 2-player support
game turns into a 2-person prisoner's dilemma (PD).
It is defined as:
\begin{equation}\label{pdcondition}
  \mbox{PD}_{i,j} : p_i(1 - p_j)(S-D) > p_j(1 - p_i)(M - H)
  \quad (\mbox{where } i \neq j \mbox{ and } s_i, s_j > 0.)
\end{equation}
The left-hand side of the PD-condition is the expected payoff if both
neighbours give support to the other.
The right-hand side is the expected payoff if neither gives support to the
other.

Hegselmann claims that the COOP-condition (eq. 6 in \cite{Hegselmann}) holds if
and only if an iterated support game has a cooperative solution for both
players.
It is defined as:
\begin{equation}\label{coopcondition}
  \mbox{COOP}_{i,j} :
  \alpha_j \geq \alpha_j^+ = {\left\{
    1 - p_j(1 - p_i) + p_i(1 - p_j) \frac{S-D}{M-H}
  \right\}}^{-1}
  \quad (\mbox{where } i \neq j \mbox{ and } s_i, s_j > 0.)
\end{equation}
The proof is unpublished and only available on request from the author.
The formula was faintly printed in our source paper \cite{Hegselmann}, but was
confirmed at \cite{website}.
Since only one value for $\alpha$ is given for each experiment, we assume that
$\alpha_j = \alpha$ for all $j$.
$\alpha$ is called the \emph{probability of stability}\footnote{
The probability of stability is denoted $\alpha$ in \cite{Hegselmann} but
$a$ in \cite{website}.} and $\alpha^+$ is the
\emph{threshold probability}\cite{website}.

A support relationship\repeatfootnote{imprecise} exists 
between two cells $C_i$ and $C_j$ (denoted $C_i \sim C_j$) if and only if both the
\emph{PD-condition} (eq. \ref{pdcondition}) and \emph{COOP-condition}
(eq. \ref{coopcondition}) holds, for both cells.
That is,
\begin{equation}\label{supportrelation}
  C_i \sim C_j \Leftrightarrow \mbox{PD}_{i,j} \mbox{ and }
  \mbox{PD}_{j,i} \mbox{ and } \mbox{COOP}_{i,j} \mbox{ and } \mbox{COOP}_{j,i}
  \quad (\mbox{where } i \neq j \mbox{ and } s_i, s_j > 0.)
\end{equation}
This definition allows describing the possibility of a support relationship
between any two cells in the lattice.
When we wish to describe the situation where two cells are in the same
neighbourhood, then we say that a \emph{functioning} support relationship exists.
This is the case when the $C_i \sim C_j$ and $C_j \in \eta_i^A$.

The support relation is symmetric, but not reflexive or transitive.
If $C_i \sim C_j$ and $C_j \sim C_k$, then clearly $C_j \sim C_i$ and
$C_k \sim C_j$.
However, it may not be true that $C_i \sim C_k$.

\subsection{Satisfaction level}
Individuals know that they are in the best possible social position that they
could possibly be in when they are surrounded only by members of the best risk
class willing to engage in support relationships with it.
They also know that they are in the worst possible position if they are
surrounded only by empty cells or cells with whom support relationship are not
possible.
A cell can therefore calculate the ratio of the expected payoff it could gain
from the neighbours in its current situation with whom support relationships
are possible against the maximum expected payoff it could possibly have.
We call this ratio the cell's satisfaction level.

We assume that the satisfaction level of a cell is only dependent on cells that
it can have a support relationship with. This yields the expected results.
If the sum were instead over all the cells in the neighbourhood, the
satisfaction level seems to remain too large for cells to become dissatisfied,
and the results diverge from those in \cite{Hegselmann}.

Let $\gamma_i(\vec{x}_k)$ denote the satisfaction
level\repeatfootnote{imprecise} of cell $C_i$ with risk class $s_i$ at position
$\vec{x}_k$.
Let
\begin{equation}\label{model2p}
  \widetilde \eta_{i,k} = \Set{\vec{x}_j \in \eta_k^A}{C_i \sim C_j}
  \quad \mbox{and} \quad
  p_i^* = \min\limits_{\vec{x}_j \in \eta_i^I} { p_j }
  % p_i^* = \min\limits_{C_j \in L} { p_j }
  \mbox{ such that } C_i \sim C_j.
\end{equation}
$\widetilde \eta_{i,k}$ is the set of neighbours that could provide support to
$C_i$, given its risk class $s_i$, at position $\vec{x}_k$.
$p_i^*$ is the probability that a cell, belonging to the best risk class
in $C_i$'s information window
that is willing to support $C_i$, becomes needy.\footnote{
We might have defined $p_i^*$ in terms of $\eta_i^I$ instead
(we found no use for $\eta_i^I$ otherwise.)}
Hence
\begin{equation}\label{model2gamma}
  \gamma_i(\vec{x}_k) = \frac{\sum\limits_{\forall \vec{x}_j
  \in \widetilde \eta_{i,k}}
  {p_i(1-p_j)(S-D)}}
  {|\eta_k^A|\ p_i(1-p_i^*)(S-D)}.
\end{equation}
It should be obvious that $\gamma_i(\vec{x}_i)$ is the satisfaction level of
$C_i$  at its current position.
Equations \ref{model2p} and \ref{model2gamma} were not given in \cite{Hegselmann}.

We call an individual satisfied if its satisfaction level is greater or equal to
some real number $\beta$, and dissatisfied otherwise.
We call $\beta$ the minimum level and it remains constant at $\beta = 0.5$.

\subsection{Migration rule}
The experimental model assumes the following migration options where locations
are compared using the satisfaction levels that an individual would have at those
locations:
\begin{enumerate}
  \item Dissatisfied individuals decide on a best alternative location.
  \item Satisfied individuals move only if the alternative location is at least
  as good as the given one.
  \item Individuals knowing that they cannot find partners at all move randomly.
\end{enumerate}

We wish to make this precise\repeatfootnote{imprecise}.
None of the equations in this subsection were given in \cite{Hegselmann}.

Since cells migrate in a random sequence, the state transition rule cannot
be analytically described as
$\phi(\eta_i): \Sigma^{|\eta_i|} \rightarrow \Sigma$.
Migration is better described by a stochastic migration rule
$\mu(\vec{x}_i): \vec{x}_i \mapsto \vec{x}_j$.

Define the best alternative location for $C_i$ as
\begin{equation}
  \vec{x}_i^* = \argmax_{\vec{x}_j \in \eta_i^M}{\gamma_i(\vec{x}_j)}
  \quad (\mbox{where } \vec{s}_j = 0).
\end{equation}
Let $\lambda(\vec{x}_i)$ be a randomly chosen $\vec{x}_j \in \eta_i^M
\mbox{ such that } \vec{s}_j = 0$.
Further, let
\begin{equation}
  m(\vec{x}_i) =
  \left\{
    \begin{array}{ll}
      % exists{\vec{x}_j seems wrong; we need x*
      %\vec{x}_i^* & \mbox{if } (\gamma_i(\vec{x}_i) < \beta \mbox{ and }
      %\exists{\vec{x}_j \in \eta_i^M}\left( C_i \sim C_j \right)) \mbox { or }
      %(\gamma_i(\vec{x}_i) \ge \beta \mbox{ and } \exists{\vec{x}_i^*}
      %\mbox{ such that } \gamma_i(\vec{x}_i^*) \geq \gamma_i(\vec{x}_i))\\
      \vec{x}_i^* & \mbox{if } \exists{\vec{x}_i^*} 
      \mbox{ such that } \left(
      \gamma_i(\vec{x}_i) < \beta=0.5 \mbox{ or }
      \gamma_i(\vec{x}_i^*) \geq \gamma_i(\vec{x}_i) \right)\\
      \lambda(\vec{x}_i) & \mbox{if } 
      \neg \exists{\vec{x}_j \in \eta_i^M}\left( C_i \sim C_j \right)\\
      \vec{x}_i & \mbox{otherwise}.
    \end{array}
  \right.
\end{equation}

Let $X_i$ be a random variable drawn from uniform distribution.
We can now define the migration rule as
\begin{equation}
\mu(\vec{x}_i \ |\ X_i) =
\left\{
  \begin{array}{ll}
    m(\vec{x}_i) & \mbox{if } X_i < \alpha \\
    \vec{x}_i & \mbox{otherwise}.
  \end{array}
\right.
\end{equation}

\subsection{Experiments}
The experiments that follow investigate what happens when the probability of
migration is increased.
We are interested in observing:
\begin{enumerate}
  \item the development of well-ordered support networks from the initial
  chaos; and

  \item the evolution of clusters with bordering phenomena.
\end{enumerate}
The three experiments described differ only with regard to the probabilities
with which individuals get migration options.
All other parameters remain constant.
According to \cite{website}, if $q$ is the probability for getting a migration
option and $\alpha$ is the probability of stability, then $\alpha = (1-q)^2$.

For each experiment, we do the following:\repeatfootnote{imprecise}
\begin{enumerate}
  \item
  At the start of each experiment, precisely $35$ empty cells are randomly
  selected and set to $i$, for $i = 1,2,\cdots,9$. This leaves a fraction
  $\frac{136}{315}=0.43$ of sites empty.

  \item
  The positions of non-empty cells are arranged in a random sequence.
  $\mu(\vec{x}_i)$ is applied to each position $\vec{x}_i$ in the sequence.
  The state at $\vec{x}_i$ is exchanged with the state of the empty cell at
  $\vec{x}_j = \mu(\vec{x}_i)$.
  That is, $s_i \leftarrow s_j = 0$ and $s_j \leftarrow s_i$.

  Each period $t$ is further divided into steps, during which only two cells are
  exchanged. So during step $u$, we have
  $s_i^{t,u+1} = s_j^{t,u} = 0$ and $s_j^{t,u+1} = s_i^{t,u}$.

  \item
  Repeat the second step 1000 times.
\end{enumerate}

Figure \ref{fig:primordialsoup} shows an example of the primordial soup that
would be the configuration of actors in each experiment after the first step.

\begin{figure}[h]
    \centering
        \begin{subfigure}{.05\textwidth}
            \centering
            \includegraphics[width=1\linewidth]{legend}
            \label{fig:legend}
        \end{subfigure}
        \begin{subfigure}{.4\textwidth}
            \centering
            \includegraphics[width=1\linewidth]{primordialsoup}
            \caption{Example of a random primordial soup}
            \label{fig:primordialsoup}
        \end{subfigure}
        \begin{subfigure}{.4\textwidth}
            \centering
            \includegraphics[width=1\linewidth]{exp1_1_infclasses_1000}
            \caption{Experiment 2.a: $q = 0.05$.}
            \label{fig:exp1_1_infclasses_1000}
        \end{subfigure}
        \begin{subfigure}{.4\textwidth}
            \centering
            \includegraphics[width=1\linewidth]{exp2_1_infclasses_1000}
            \caption{Experiment 2.b: $q = 0.10$}
            \label{fig:exp2_1_infclasses_1000}
        \end{subfigure}
        \begin{subfigure}{.4\textwidth}
            \centering
            \includegraphics[width=1\linewidth]{exp3_1_infclasses_1000}
            \caption{Experiment 2.c: $q = 0.15$}
            \label{fig:exp3_1_infclasses_1000}
        \end{subfigure}
    \captionsetup{singlelinecheck=off}
    \caption[boo]{
    Results from experiments 2.a--c.
    \ref{fig:primordialsoup} illustrates a randomized start configuration
    for one of the experiments, called a primordial soup.
    The other images show the lattice configuration after 1000 steps using
    various values of $q$.
    
    Different colours represent the different risk classes;
    white cells are empty;
    round cells are dissatisfied;
    filled cells are satisfied; and
    white lines connecting two individuals indicate functioning support
    relations between them.
    
    \paragraph{\ref{fig:exp1_1_infclasses_1000}}
    Members of class 1 establish support relationships mostly among 
    themselves, although there also exist relationships between members of class
    1 and 2.
    Members of class 2 live mostly around those of class 1.
    Members of class 3 live mostly around those of class 2 and so forth.
    Members of class 9 could only find partners of class 8 and 9.
      
    \paragraph{\ref{fig:exp2_1_infclasses_1000}}
    Members of the best and worst risk classes only form support relationships
    amongst themselves.
    Members of class 2 now have a similar role as the members of
    risk class 1 during experiment $a$ and form relationships mostly among
    themselves, but there also exist relationships between members of class 2 and
    class 3.
    Members of class 3 live mostly around those of class 2.
    Members of class 4 live mostly around those of class 3 and so forth.

    \paragraph{\ref{fig:exp3_1_infclasses_1000}}
    Members of the best and worst risk classes could not form support
    relationships at all.
    It is now the members of the second best and second worst
    (classes 2 and 8) that form support relationships only amongst themselves.
    Members of class 3 now have a similar role as the members of
    risk class 2 during experiment $b$ and form relationships mostly among
    themselves, but there also exist relationships between members of class 3 and
    class 4.
    Members of class 4 live mostly around those of class 3, etc.
    }
   
    \label{fig:migration_results}
\end{figure}

\subsection{Analysis}
The structures presented in our results all resemble their corresponding
figures in \cite{Hegselmann}, which gives us some confidence that the experiments
were reproduced well.
There are some differences though, which are discussed next.

\subsubsection{Number of migration options used}
\begin{figure}[h]%[tb]
  \centering
  \begin{tabular}{ccl}

  Exp. & \# (us) & \# (Hegselmann) \\
  \hline
  1 & 3733 & 7249 \\
  2 & 5589 & 9490 \\
  3 & 15379 & 15139
  \end{tabular}
 
  \caption{Number of migrations options actually used in each experiment.
  The second column lists the numbers we counted and the third lists
  the numbers as counted in \cite{Hegselmann}.
  In the first two rows Hegselmann seems to count twice as many,
  but in the last row we count about the same number of migrations.}
  \label{fig:migrations}
\end{figure}

We noted a discrepency between the number of migrations we observed with the
number stated in \cite{Hegselmann} (see Fig. \ref{fig:migrations}).
Similar results were found when the experiments were repeated.
We increment the count whenever a cell moves to an empty location.
One might consider the possibility that Hegselmann counts the migration
options twice, by regarding the movement of the empty cell as another migration
option.
This would explain why we count only half as many migrations in experiments
$a$ and $b$.
However, in experiment $c$, our count is close to Hegselmann's.
Therefore this hypothesis does not hold and the cause of the discrepancy
remains unknown.

\section{Conclusion}
We found that \cite{Hegselmann} was lacking many important details which made
replicating Hegselmann's results difficult.
Apart from his website\cite{website}, we failed to find similar work in
literature with which to compare his paper.
We proceeded by formalizing various concepts that were presented.
The fact that many parameters and assumptions made by Hegselmann were both
implicit and arbitrary meant that we had to make our own assumptions and derive
equations as we saw fit.
Because we were unable to reproduce the results with certainty, we refrain
from making inferences on real-world social phenomena based on these experiments.

We had dubious success with the fixed-site model, and had to change some 
parameter settings to make any progress.
There appear to be unresolved issues surrounding the initial conditions.
These may be the cause of the differences we observed and explain why
the number of cooperators in experiment 1.c began oscillating.

We had more success with the migration model.
Our results appear to agree with those in \cite{Hegselmann}, except for
the number of migrations that actually took place.

Finally, the nature of the experiments is such that the computations cannot be
parallelized as with most cellular automata.
This means that these experiments do not scale well and larger experiments
could take a significant amount of time to compute.
This detracts from the usefulness of these models to explain complex, real-world
phenomena.
However, this could be prevented by devising new models of these situations
that do not exhibit such dependencies between cells during each time step.
\bibliography{bib}{}
\bibliographystyle{plain}
\end{document}
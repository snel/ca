/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca

import scala.collection.mutable.ArrayBuffer

/** A cellular automaton neighbourhood.
  *
  * @note Cell shapes are assumed to be hyper-cubical (squares, cubes etc.).
  * Not sure if we can adapt this to support hyper-hexagonal cell shapes, for example.
  *
  * @define inclusive includes the cell in question
  * @define wrapping Not periodic: cells on boundaries do not wrap around, the neighbourhood is cropped instead.
  * @define neighbourhood neighbourhood
  */
abstract class Neighbourhood {
  /** Returns cells in the neighbourhood of the given cell. */
  def apply(x: Seq[Int], lattice: Lattice[_]): Iterator[Seq[Int]]
}

/** Provides methods for extracting cubical neighbourhoods.
  *
  * @define shape cubical
  * @define r {half-width} - 1/2
  * @define almostRadius The parameter `r` is almost the half-width of the $shape neighbourhood.
  */
trait Cubical { self: Neighbourhood =>
  /** Returns a cubical neighbourhood around x, cropped (not wrapped) at the
    * boundaries of the lattice.
    *
    * @param x centre of the cube
    * @param lattice lattice the cube is constructed in
    * @param r `radius`
    */
  protected def cube(x: Seq[Int], lattice: Lattice[_], r: Int) = {
    val ranges = x.zip(lattice.dims).map {
      case (i, ubound) =>
        var lo = i - r
        var hi = i + r
        if (lo < 0) lo = 0
        if (hi >= ubound) hi = ubound - 1
        lo to hi
    }
    SeqMath.cartesian(ranges) // Maps a Seq of ranges to a Seq of coordinates.
  }
}

/** Provides documentation parameters for spherical neighbourhoods.
  *
  * @define shape spherical
  * @define r {radius} - 1/2
  * @define almostRadius The parameter `r` is almost the radius of the $shape neighbourhood.
  */
trait Spherical { self: Neighbourhood => }

/** Provides methods for extracting neighbourhoods that wrap around lattice
  * boundaries. Stated differently, the neighbourhood is periodic or the
  * lattice is toroidal.
  *
  * The Cubical trait must also be mixed in for this to work.
  * This trait should be mixed in last.
  *
  * @define wrapping Periodic: cells on boundaries wrap around.
  */
trait Periodic extends Neighbourhood { self: Cubical =>
  /** Returns a cubical neighbourhood around x, not cropped at the
    * boundaries of the lattice. The coordinates returned may be negative.
    *
    * @param x centre of the cube
    * @param lattice lattice the cube is constructed in; unused
    * @param r `radius`
    */
  override protected def cube(x: Seq[Int], lattice: Lattice[_], r: Int) = {
    val ranges = x.map { i =>
      var lo = i - r
      var hi = i + r
      lo to hi
    }
    SeqMath.cartesian(ranges) // Maps a Seq of ranges to a Seq of coordinates.
  }

  /** Wraps each element of xs around the boundaries of the lattice.
    *
    * @param xs sequence of coordinates
    * @param lattice lattice the coordinates are from
    */
  protected def periodic(xs: Iterator[Seq[Int]], lattice: Lattice[_]): Iterator[Seq[Int]] = {
    xs.map {
      _.zip(lattice.dims).map {
        case (z, d) => (z + d) % d // wraps around; modulus preserves sign in Scala
      }
    }
  }

  override abstract def apply(x: Seq[Int], lattice: Lattice[_]) = {
    val m = super.apply(x, lattice)
    periodic(m, lattice)
  }
}

/** $neighbourhood -- $inclusive
  *
  * $wrapping
  * $almostRadius
  *
  * @param r $r
  * @define neighbourhood Moore neighbourhood
  */
class Moore(val r: Int) extends Neighbourhood with Cubical {
  override def apply(x: Seq[Int], lattice: Lattice[_]) = cube(x, lattice, r)
}

/** Companion for Moore. */
object Moore {
  /** Returns a Moore neighbourhood.
    *
    * @param wrapped if true, the neighbourhood wraps around lattice boundaries
    */
  def apply(r: Int, periodic: Boolean = false) = if (periodic) new Moore(r) with Periodic else new Moore(r)
}

/** $neighbourhood -- $inclusive
  *
  * $wrapping
  * $almostRadius
  *
  * @param r $r
  * @define neighbourhood Von Neumann neighbourhood
  */
class VonNeumann(val r: Int) extends Neighbourhood with Cubical with Spherical {
  import SeqMath._
  val r2 = r * r

  /** Returns a Von Neumann neighbourhood. */
  override def apply(x: Seq[Int], lattice: Lattice[_]) =
    cube(x, lattice, r).filter { y => (x - y).normSquared <= r2 } // sqrt is expensive, comparing squares is faster
}

/** Companion for VonNeumann. */
object VonNeumann {
  /** Returns a Von Neumann neighbourhood.
    *
    * @param periodic if true, the neighbourhood wraps around lattice boundaries
    */
  def apply(r: Int, periodic: Boolean = false) = if (periodic) new VonNeumann(r) with Periodic else new VonNeumann(r)
}
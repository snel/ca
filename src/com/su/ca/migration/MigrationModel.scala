package com
package su
package ca
package migration

/**
 * Actor in the migration model on the grid.
 *
 * @param state: One of {0,1,...,9} states that indicate risk class to get needy.
 * @param satisfied: Whether the actor is satisfied (satisfaction level is higher than the minimum value) at its
 * current position.
 * @param rel: Array of length 4 where every entry in the array indicates whether the actor is currently in a
 * support relationship with the actors to it's top, left, bottom and right, respectively.
 */
case class MigrationModel(val state: Int, val satisfied: Boolean = false, val rel: Seq[Boolean] = Seq(false, false, false, false)) {
  require(state >= 0)
  require(rel.length == 4)
  override def toString = state.toString
}
package com
package su
package ca
package migration

import gui.GUI
import gui.MigrationGrid
import gui.Grid
import scala.util.Random
import scala.math.pow
import SeqMath._
import com.su.ca.tests.performance.PrintTimed

/** Runs experiments on the migration model. */
object MigrationExperiments extends App {
  GUI.main(Array.empty)
  experiment1()

  def run(q: Double, size: Int, numPeriods: Int): Unit = {
    val lattice = Lattice[MigrationModel](MigrationModel(0))(IndexedSeq(size, size))
    val rule = MigrationRule(q)

    // Select 35 random empty cells and set to state s.
    val xs = scala.util.Random.shuffle(lattice.cells)
    for (s <- 1 to 9) {
      for (i <- 1 to 35) {
        lattice(xs.next()) = MigrationModel(s)
      }
    }
    lattice.apply()

    // Initializes the GUI.
    //    GUI.fromSwingThread {
    GUI.updateGridPanel(MigrationGrid(lattice, rule))
    //    }

    // Runs the experiment.
    // We could use the `play` functionality directly, but only if we make use of GUI.step() here.
    PrintTimed {
      for (p <- 0 to numPeriods) {
        // Waiting optional; animates the progression
        //        GUI.synchronized {
        //          GUI.wait(15)
        //        }

        GUI.step()
      }
    }
  }

  def experiment1(): Unit = {
    run(0.05, 21, 1000)
  }

  def experiment2(): Unit = {
    run(0.10, 21, 1000)
  }

  def experiment3(): Unit = {
    run(0.15, 21, 1000)
  }
}

/******************************************** RULE ***********************************************/

/** Rule used by experimental model 2, a.k.a. the migration model.
  */
class MigrationRule(val q: Double) extends Rule[MigrationModel] {
  private val informationNeighbourhood = Moore(6, periodic = true)
  private val migrationNeighbourhood = Moore(5, periodic = true)
  private val interactionNeighbourhood = VonNeumann(1, periodic = true)
  private val minLev = 0.5
  private val D = 1
  private val S = 5
  private val H = 6
  private val M = 7
  private var numMigrationsUsed = 0
  private var numSteps = 0

  /** Support relation possibility between any two states. */
  private val supportRelations = {
    val alpha = (1 - q) * (1 - q)

    /** Tests the forward condition for PD_i,j and COOP_i,j */
    def y(p0: Double, p1: Double) = {
      p0 * (S - D) > p1 * (M - H) && // PD-condition
        1./(1 - p0 + p1 * (S - D) / (M - H)) <= alpha // COOP-condition
    }

    (1 to 9).flatMap { s_i =>
      (1 to 9).map { s_j =>
        val p_i = 0.1 * s_i
        val p_j = 0.1 * s_j
        val a = p_i * (1 - p_j)
        val b = p_j * (1 - p_i)
        (s_i, s_j) -> (y(a, b) && y(b, a))
      }
    }.toMap
  }

  /** The class probability of the best risk class willing to engage in a
    * support relationship with a given class.
    */
  private val p_i_star = {
    (1 to 9).map { s_i =>
      var s_i_star = 9
      while (s_i_star > 1 && willSupport(s_i, s_i_star - 1)) {
        s_i_star -= 1
      }
      (s_i) -> 0.1 * s_i_star
    }.toMap
  }

  /** Calculate the class probability of the best risk class willing to engage in a
    * support relationship with a given class.
    */
  private def calc_p_i_star(s_i: Int, x_i: Seq[Int], lattice: BufferedLattice[MigrationModel]): Double = {
    // Calculate using all possible risk classes.
    //  p_i_star.getOrElse((s_i), 0.0)
    //     Calculate using only the risk classes in the cell's information window.
        val N_ii = informationNeighbourhood(x_i, lattice).filter { x => lattice(x).state != 0 }.toSeq
        var s_star = 9
        N_ii.foreach { x_j =>
          val s_j = lattice(x_j).state
          if (s_j < s_star && willSupport(s_j, s_i)) {
            s_star = s_j
          }
        }
        0.1 * s_star
  }

  /** Returns true iff C_i ~ C_j.
    *
    * @note Of course, if i and j aren't in the same neighbourhood there
    * wouldn't be an actual support relationship.
    */
  private def willSupport(s_i: Int, s_j: Int) = supportRelations.getOrElse((s_i, s_j), false)

  /** Calculate the satisfaction
    * level, gamma_i for class s_i at lattice position x_k.
    */
  private def gamma_i(s_i: Int, x_k: Seq[Int], lattice: BufferedLattice[MigrationModel]): Double = {
    val p_i = 0.1 * s_i
    val N_ia = interactionNeighbourhood(x_k, lattice).filter { _ != x_k }.toSeq // Remove the cell itself.

    // Calculate p_i*.
    val p_i_s = calc_p_i_star(s_i, x_k, lattice)

    // Determine the expected payoff from the cell's interaction window.
    var exp_pay = 0.0
    N_ia.foreach { x_j =>
      // Using only neighbours it can form support relationships with.
      if (willSupport(s_i, lattice(x_j).state)) {
        val p_j = 0.1 * lattice(x_j).state
        exp_pay += p_i * (1 - p_j) * (S - D)
      }

      // Using all neighbours.
      //      val p_j = 0.1 * lattice(x_j).state
      //      exp_pay += Math.max(p_i * (1 - p_j) * (S - D), p_j * (1 - p_i) * (M - H))
    }

    exp_pay / (N_ia.length * p_i * (1.0 - p_i_s) * (S - D))
  }

  override def apply(lattice: BufferedLattice[MigrationModel]) = {
    numSteps += 1
    val xs = scala.util.Random.shuffle(lattice.cells).toSeq
    xs.foreach { x_i =>
      // Generate random number to give some cells the option to migrate.
      val X_i = scala.util.Random.nextDouble()
      if (lattice(x_i).state != 0 && X_i < q) { // Non-empty cell gets the option to migrate.
        val N_im = migrationNeighbourhood(x_i, lattice).filter { x => lattice(x).state == 0 && x != x_i }.toSeq // Empty cells in migration neighbourhood
        // val N_ii = interactionNeighbourhood(x_i, lattice).filter { _ != x_i }.toSeq
        val s_i = lattice(x_i).state

        // Calculate C_i's satisfaction level, gamma_i(x_i).
        val gamma = gamma_i(s_i, x_i, lattice)

        val satisfied = gamma >= minLev
        // Check if actor has potential partners in its interaction neighbourhood.
        val hasPartners = gamma != 0
        var newPos = x_i

        if (satisfied || hasPartners) {
          // Find best alternative (empty) location, i.e. the location with the highest satisfaction level in the cell's migration neighbourhood.
          var max_sat = 0.0
          var x_i_star = x_i
          N_im.foreach { x_j =>
            val x_j_gamma = gamma_i(s_i, x_j, lattice)
            if (x_j_gamma > max_sat) {
              max_sat = x_j_gamma
              x_i_star = x_j
            }
          }
          if (satisfied && max_sat < gamma) {
            newPos = x_i
          } else {
            newPos = x_i_star
          }
        } else {
          // Find random location.
          newPos = N_im(scala.util.Random.nextInt(N_im.length))
        }

        if (newPos != x_i) { // Move actor.
          numMigrationsUsed += 1
          lattice(newPos) = MigrationModel(lattice(x_i).state)
          lattice(x_i) = MigrationModel(0)
          // Applies the change immediately; thus cell updates are not independent.
          lattice.applyAt(x_i)
          lattice.applyAt(newPos)
        }
      }
    }

    // Check relationships & satisfaction levels.
    lattice.cells.foreach { x_i =>
      if (lattice(x_i).state != 0) {
        val s_i = lattice(x_i).state
        // Calculate C_i's satisfaction level, gamma_i(x_i).
        val gamma = gamma_i(s_i, x_i, lattice)
        val satisfied = gamma >= minLev
        var rel = Array(false, false, false, false)
        val h = lattice.dims(0)
        val x_t = Seq((x_i(0) - 1 + h) % h, x_i(1))
        val x_l = Seq(x_i(0), (x_i(1) - 1 + h) % h)
        val x_b = Seq((x_i(0) + 1) % h, x_i(1))
        val x_r = Seq(x_i(0), (x_i(1) + 1) % h)
        if (willSupport(lattice(x_t).state, s_i)) {
          rel(0) = true
        }
        if (willSupport(lattice(x_l).state, s_i)) {
          rel(1) = true
        }
        if (willSupport(lattice(x_b).state, s_i)) {
          rel(2) = true
        }
        if (willSupport(lattice(x_r).state, s_i)) {
          rel(3) = true
        }
        lattice(x_i) = MigrationModel(lattice(x_i).state, satisfied, rel)
      }
    }
    //    println("Step: " + numSteps + ", migration options used so far: " + numMigrationsUsed)
  }
}

/** Companion for MigrationRule. */
object MigrationRule {
  def apply(q: Double) = new MigrationRule(q)
}
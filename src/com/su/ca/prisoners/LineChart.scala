package com.su.ca.prisoners

import org.jfree.chart._
import org.jfree.data.xy._
import org.jfree.chart.plot.PlotOrientation

object LineChart extends App {

def draw_Graph (xpos: Array[Double], percent: Array[Double], experiment: Int) {
  val x = xpos
  val coop = percent
  val y = x.map(_ * 3)
  val i = "" + experiment
  //val dataset = new DefaultXYDataset
  //dataset.addSeries("Series 1", Array(x, y))

  val chart = ChartFactory.createXYLineChart(
         "Cooperative Decisions made for experiment (" + i + ")",
         "Periods","Cooperative Percentage",
         createDataset(x, coop),
         PlotOrientation.VERTICAL,
         true,true,false);
  
  val plot = chart.getXYPlot();
  val rangeAxis = plot.getRangeAxis();
  rangeAxis.setRange(0.0, 100.0);
      
  val frame = new ChartFrame(
    "Graph for experiment (" + i + ")" ,
    chart)
  
  frame.pack()
  frame.setVisible(true)

}
  def createDataset(points: Array[Double], values: Array[Double]): XYDataset =
   {
	   val dataset = new XYSeriesCollection();
	   val series1 = new XYSeries("Cooperative Decisions")
	   series1.add(points(0),values(0))
	   var x = 0
	   
	   for (x <- 1 until points.length) {
	     series1.add(points(x), values(x))
	   }
	    
	   dataset.addSeries(series1);
	   dataset
   }
}
package com
package su
package ca
package prisoners

/** Prisoner is an entry on our grid
  *
  * Each prisoner has two accounts, one for cooperation, and one for defection.
  * Cooperations are cases where prisoners do not make a deal with the cops.
  * Defections are cases where prisoners do make deals with cops.
  *
  * @param state 1: cooperative, 0: defective
  */
case class Prisoner(val state: Int, val defacc: Double = 0, val coopacc: Double = 0) {
  require(defacc >= 0)
  require(coopacc >= 0)
  override def toString = state.toString
}
package com
package su
package ca
package prisoners

import gui.GUI
import gui.PrisonerGrid
import gui.Grid
import scala.util.Random
import scala.math.pow
import SeqMath._
import com.su.ca.tests.performance.PrintTimed
import java.io.BufferedWriter
import java.io.FileWriter
import java.io.File
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import javax.swing.SwingUtilities
import java.io.FileSystem
import java.io.FileSystem

/** Contains the 3 experiments for experimental model 1, the
  * prisoners dilemma.
  */
object PrisonerExperiments extends App {
  GUI.main(Array.empty)

  experiment_a_i()
  experiment_a_ii()
  experiment_a_iii()

  experiment_b_i()
  experiment_b_ii()
  experiment_b_iii()

  experiment_c_i()
  experiment_c_ii()
  experiment_c_iii()

  def run(neighourhood: Neighbourhood, k: Double, alpha: Double, kappa: Double, size: Int, numPeriods: Int) = {
    val lattice = Lattice[Prisoner](new Prisoner(0))(IndexedSeq(size, size)) //(IndexedSeq(50, 50))
    val rule = PrisonerRule(neighourhood, k, alpha, kappa)
    val positions = new Array[Double](numPeriods + 1)
    val percent = new Array[Double](numPeriods + 1)
    val grid_size = size * size

    // Sets half of the cells as being cooperative.
    val xs = scala.util.Random.shuffle(lattice.cells).toSeq
    xs.take(xs.length / 2).foreach { x =>
      lattice(x) = new Prisoner(1)
    }
    lattice.apply()

    // Initializes the GUI.
    //    GUI.fromSwingThread {
    GUI.updateGridPanel(PrisonerGrid(lattice, rule))
    GUI.step()
    //    }

    // Runs the experiment.
    // We could use the `play` functionality directly, but only if we make use of GUI.step() here.
    PrintTimed {
      for (p <- 0 to numPeriods) {
        // Waiting optional; animates the progression
        //        GUI.synchronized {
        //          GUI.wait(200)
        //        }

        val coop = lattice.cells.count { lattice(_).state == 1 }
        positions(p) = p
        percent(p) = (coop.toDouble / grid_size.toDouble) * 100
        GUI.step()
      }
    }
    //    LineChart.draw_Graph(positions, percent, 3)
    percent
  }

  def saveData(data: Array[Double], filename: String): Unit = {
    new File(s"exp1").mkdir()
    val w = new BufferedWriter(new FileWriter(s"exp1/$filename.dat"))

    try {
      w.write("Period, Cooperators\n")
      data.zipWithIndex.foreach {
        case (p, i) =>
          w.write("%d, %2.0f\n".format(i + 1, p))
      }
    } finally {
      w.close()
    }

    // Renders a screenshot directly.
    SwingUtilities.invokeAndWait {
      new Runnable() {
        def run() = {
          val p = GUI.gridPanel
          val image = new BufferedImage(
            p.getWidth,
            p.getHeight,
            BufferedImage.TYPE_INT_RGB)
          p.paint(image.getGraphics)

          ImageIO.write(
            image,
            "png",
            new File(s"exp1/$filename.png"))
        }
      }
    }
  }

  def experiment_a_i(): Unit = {
    println("1.a.i")
    val data = run(VonNeumann(1, periodic = true), 0, 0, 1, 50, 75)
    saveData(data, "1ai")
  }

  def experiment_a_ii(): Unit = {
    println("1.a.ii")
    val data = run(VonNeumann(1, periodic = true), 0, 0, 0.01, 50, 75)
    saveData(data, "1aii")
  }

  def experiment_a_iii(): Unit = {
    println("1.a.iii")
    val data = run(VonNeumann(1, periodic = true), 0, 0.15, 0.01, 50, 75)
    saveData(data, "1aiii")
  }

  def experiment_b_i(): Unit = {
    println("1.b.i")
    val data = run(Moore(1, periodic = true), -1, 0.7, 1, 50, 75)
    saveData(data, "1bi")
  }

  def experiment_b_ii(): Unit = {
    println("1.b.ii")
    val data = run(Moore(1, periodic = true), -1, 0.7, 0.01, 50, 75)
    saveData(data, "1bii")
  }

  def experiment_b_iii(): Unit = {
    println("1.b.iii")
    val data = run(Moore(1, periodic = true), -1, 0.25, 0.01, 50, 75)
    saveData(data, "1biii")
  }

  def experiment_c_i(): Unit = {
    println("1.c.i")
    val data = run(Moore(3, periodic = true), -2, 0.5, 1, 50, 75)
    saveData(data, "1ci")
  }

  def experiment_c_ii(): Unit = {
    println("1.c.ii")
    val data = run(Moore(3, periodic = true), -2, 0.5, 0.01, 50, 75)
    saveData(data, "1cii")
  }

  def experiment_c_iii(): Unit = {
    println("1.c.iii")
    val data = run(Moore(3, periodic = true), -2, 0.099, 0.01, 50, 75)
    saveData(data, "1ciii")
  }
}

/******************************************** RULE ***********************************************/

/** Rule used by experimental model 1, a.k.a. the prisoner model.
  * Do not attempt to parallelize this.
  */
class PrisonerRule(neighbourhood: Neighbourhood,
    val k: Double,
    val alpha: Double,
    val beta: Double) extends Rule[Prisoner] {

  /** Weighted sum of distances from center cell x_i to each cell in s. */
  private def w(x_i: Seq[Int], s: Seq[Seq[Int]]): Double =
    s.map { x_j => Math.pow((x_i - x_j).norm, k) }.sum

  override def apply(lattice: BufferedLattice[Prisoner]) = {
    // Selects half of the prisoners, and updates them.
    val xs = scala.util.Random.shuffle(lattice.cells).toSeq
    xs.take(xs.length / 2).foreach { x_i =>
      // The set of actors around C_i.
      // Excludes C_i, because its distance is always 0. 
      val N_i = neighbourhood(x_i, lattice).filter { _ != x_i }.toSeq

      // The set of cooperating actors around C_i.
      val C1_i = N_i.filter { lattice(_).state == 1 }

      // Calculates relative frequency.
      val q_i = w(x_i, C1_i) / w(x_i, N_i)

      // Neither of max/mean yields good results.
      val h_1 = N_i.map { lattice(_).defacc }.max
      val h_2 = N_i.map { lattice(_).coopacc }.max
      val h = if (h_2 >= h_1) 1 else 0

      val v = lattice(x_i)

      if (v.state > 0) {
        lattice(x_i) = new Prisoner(h, v.defacc, alpha * v.coopacc + 0.1 * q_i)
      } else {
        lattice(x_i) = new Prisoner(h, alpha * v.defacc + 0.1 * q_i + beta, v.coopacc)
      }
    }
  }
}

/** Companion for PrisonerRule. */
object PrisonerRule {
  def apply(neighbourhood: Neighbourhood, k: Double, alpha: Double, kappa: Double = 1) =
    new PrisonerRule(neighbourhood, k, alpha, kappa)
}
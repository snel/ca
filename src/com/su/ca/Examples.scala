/*
 * CA project
 * @author S. E. A. Nel
 * @author D. D. Malan
 */

package com.su.ca

import java.io.File

/** Example application. */
object Examples extends App {
  import Lattice._ // Allows implicit use of Lattice2D.tabulate

  /* Examples of how to use the framework ************************************/

  new Example("An example of getting/setting values") {
    val lattice = Lattice[Int](0)(IndexedSeq(1, 2, 3)) // 3D non-square lattice
    val x = Seq(0, 0, 0)

    demo { lattice(x) = 42 } { println(lattice(x)) }
  }

  new Example("Same example using direct notation") {
    val lattice = Lattice[Int](0)(IndexedSeq(1, 2, 3))

    demo { lattice(Seq(0, 0, 0)) = 42 } { println(lattice(0, 0, 0)) }
  }

  new Example("Application of rule 42 (without any neighbourhood)") {
    val lattice = Lattice[Int](0)(IndexedSeq(1, 2, 3))

    demo { Rule42.apply(lattice) } {
      println(lattice(0, 0, 0))
      println(lattice(0, 1, 2))
    }
  }

  new Example("Example of directly affecting cells in a Moore neighbourhood") {
    val lattice = Lattice[Int](0)(IndexedSeq(3, 4))
    val x = Seq(0, 2)

    demo {
      Moore(1)(x, lattice) foreach { lattice(_) = 1 }
    } { println(lattice.tabulate) }
  }

  new Example("Application of a Moore neighbourhood rule") {
    val lattice = Lattice[Int](0)(IndexedSeq(5, 5))
    val x = Seq(0, 1)
    lattice(x) = 1
    lattice.apply()

    demo { Next42(Moore(2)).apply(lattice) } { println(lattice.tabulate) }
  }

  new Example("Application of a Von Neumann neighbourhood rule") {
    val lattice = Lattice[Int](0)(IndexedSeq(8, 8))
    val x = Seq(2, 3)
    lattice(x) = 1
    lattice.apply()

    demo { Next42(VonNeumann(2)).apply(lattice) } { println(lattice.tabulate) }
  }

  new Example("Application of a wrapped Moore neighbourhood rule") {
    val lattice = Lattice[Int](0)(IndexedSeq(8, 8))
    val x = Seq(1, 1)
    lattice(x) = 1
    lattice.apply()

    demo { Next42(Moore(2, periodic = true)).apply(lattice) } {
      println(lattice.tabulate)
    }
  }

  new Example("Application of a wrapped Von Neumann neighbourhood rule") {
    val lattice = Lattice[Int](0)(IndexedSeq(8, 8))
    val x = Seq(1, 1)
    lattice(x) = 1
    lattice.apply()

    demo { Next42(VonNeumann(3, periodic = true)).apply(lattice) } {
      println(lattice.tabulate)
    }
  }

  new Example("Application of a rule on a lattice of strings") {
    val lattice = Lattice[String]("bar")(IndexedSeq(3, 3))
    val x = Seq(0, 1)
    lattice(x) = "foobar"
    lattice.apply()

    demo { Foobar.apply(lattice) } { println(lattice.tabulate(6)) }
  }

  new Example("Example of specialized boolean lattice") {
    val lattice = Lattice.boolean()(IndexedSeq(3, 3))
    val x = Seq(0, 1)
    lattice(x) = true
    lattice.apply()
    println(lattice.tabulate)
  }

  new Example("Game of life blinker example") {
    val lattice = Lattice.boolFromFile("examples/blinker.txt")

    demo { ConwayBoolean(Moore(1)).apply(lattice) } {
      println(lattice.tabulate)
    }
  }

  new Example("Game of life toad example") {
    val lattice = Lattice.boolFromFile("examples/toad.txt")

    demo { ConwayBoolean(Moore(1)).apply(lattice) } {
      println(lattice.tabulate)
    }
  }
}

/* Some example rules ********************************************************/

object Rule42 extends Rule[Int] {
  /** Simply sets all states to 42! No neighbourhood required. */
  override def apply(lattice: BufferedLattice[Int]) = {
    lattice.cells.foreach { lattice(_) = 42 }
  }
}

object Foobar extends Rule[String] {
  /** Sets all states to "baz" if they contain the substring "foo", otherwise they become "yum". */
  override def apply(lattice: BufferedLattice[String]) = {
    lattice.cells.foreach { x =>
      lattice(x) = if (lattice(x).contains("foo")) "baz" else "yum"
    }
  }
}

class Next42(neighbourhood: Neighbourhood) extends Rule[Int] {
  /** Sets all states to 42, if any positive values are in the neighbourhood. */
  override def apply(lattice: BufferedLattice[Int]) = {
    lattice.dividedCells.foreach { // this makes the rule work concurrently
      _ foreach { x =>
        if (pred(x, lattice)) lattice(x) = 42
      }
    }
  }

  def pred(x: Seq[Int], lattice: BufferedLattice[Int]) =
    neighbourhood(x, lattice).exists { y => x != y && lattice(y) > 0 }
}

object Next42 {
  def apply(neighbourhood: Neighbourhood) = new Next42(neighbourhood)
}

/* Utilities *****************************************************************/

/** Example demonstration workflow.
  */
abstract class Example(title: String) {
  def lattice: BufferedLattice[_]

  println("-" * 80)
  println(title)

  /** 1. shows the lattice before the action takes place (with the `show` method)
    * 2. executes the `action` block
    * 3. saves changes to the lattice performed during previous step
    * 4. show the lattice after the action takes place
    *
    * @param action action to perform
    * @param show method of showing state of the lattice
    */
  def demo(action: => Any)(show: => Any = ()) {
    println("before:")
    show
    action
    lattice.apply()
    println("after:")
    show
  }
}
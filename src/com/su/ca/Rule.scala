/*
 * CA project
 * @author S. E. A. Nel
 * @author D. D. Malan
 */

package com.su.ca

/** A rule applicable to a lattice of cellular automata.
  *
  * @tparam A cell state type
  */
abstract class Rule[A] {
  /** Applies this rule to the lattice. */
  def apply(lattice: BufferedLattice[A])
}

/** Conway's game-of-life rule for Int lattices.
  *
  * The neighbourhood is usually a Moore neighbourhood of 9 cells, i.e. r=1.
  * The cell in question is treated as not part of the neighbourhood.
  */
class Conway(neighbourhood: Neighbourhood) extends Rule[Int] {
  /** Any live cell with fewer than two live neighbours dies, as if caused by under-population.
    * Any live cell with two or three live neighbours lives on to the next generation.
    * Any live cell with more than three live neighbours dies, as if by over-population.
    * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    */
  override def apply(lattice: BufferedLattice[Int]) = {
    lattice.dividedCells.foreach { // this makes the rule work concurrently
      _ foreach { x =>
        val s = neighbourhood(x, lattice).map { y => if (x != y) lattice(y) else 0 }.sum // counts the live neighbours, not including x
        lattice(x) = lattice(x) match {
          case 0 => if (s == 3) 1 else 0
          case 1 => if (s == 2 || s == 3) 1 else 0
        }
      }
    }
  }
}

/** Companion for Conway's game-of-life rule. */
object Conway {
  def apply(neighbourhood: Neighbourhood) = new Conway(neighbourhood)
}

/** Conway's game-of-life rule for boolean lattices.
  *
  * The neighbourhood is usually a Moore neighbourhood of 9 cells, i.e. r=1.
  * The cell in question is treated as not part of the neighbourhood.
  */
class ConwayBoolean(neighbourhood: Neighbourhood) extends Rule[Boolean] {
  /** Any live cell with fewer than two live neighbours dies, as if caused by under-population.
    * Any live cell with two or three live neighbours lives on to the next generation.
    * Any live cell with more than three live neighbours dies, as if by over-population.
    * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    */
  override def apply(lattice: BufferedLattice[Boolean]) = {
    lattice.dividedCells.foreach { // this makes the rule work concurrently
      _ foreach { x =>
        val s = neighbourhood(x, lattice).map { y => if (x != y && lattice(y)) 1 else 0 }.sum // counts the live neighbours
        lattice(x) = lattice(x) match {
          case false => s == 3
          case true => s == 2 || s == 3
        }
      }
    }
  }
}

/** Companion for Conway's game-of-life rule for boolean lattices. */
object ConwayBoolean {
  def apply(neighbourhood: Neighbourhood) = new ConwayBoolean(neighbourhood)
}
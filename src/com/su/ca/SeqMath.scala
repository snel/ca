package com.su.ca

import scala.reflect.ClassTag

/** Provides mathematical functions on sequences. */
object SeqMath {
  /** Cartesian product.
    *
    * @note This returns an iterator, so take care to reset it when it has
    * to be used more than once. Also, iterators are not thread-safe -- don't
    * use the same iterator on more than one thread at a time.
    *
    * @note On performance:
    * <li> prefer iterators to collections, because collections load all their
    * values in memory at once.
    * <li> prefer iterators to streams, because although
    * streams cache accessed values, this also implies that if one iterates
    * over the whole stream, all the values will still be in memory at once.
    * <li> iterators don't permit concurrent access directly. One can however,
    * convert iterators to collections and then hand them off to threads.
    *
    * @example NxN = (x, y) in N^2.
    */
  final def cartesian[A](xs: Seq[Seq[A]]): Iterator[Seq[A]] = {
    if (xs.isEmpty) Iterator(Seq.empty)
    else xs.head.iterator.flatMap { i => cartesian(xs.tail).map(i +: _) }
  }

  /** Vector operations on sequences. Value class -- doesn't cause overhead. */
  implicit class VecOps[A](val x: Seq[A]) extends AnyVal {
    /** Returns the dot product xy.
      *
      * @note We use functional programming instead of a loop because
      * the vector need not be an indexed sequence, such as an array.
      * Also, `fold` is faster than `map.sum`, because it doesn't create
      * an intermediate list.
      */
    def dot(y: Seq[A])(implicit num: Numeric[A]) = {
      import num._
      x.zip(y).foldLeft(num.zero) { (sum, v) => sum + v._1 * v._2 }
    }

    /** Returns the L2 norm, squared.
      *
      * Because this function is generic, a small amount of overhead is added
      * during type conversion.
      */
    def normSquared(implicit num: Numeric[A]) = {
      import num._
      x.foldLeft(num.zero) { (sum, i) => sum + i * i }
    }

    /** Returns the L2 norm.
      *
      * Because this function is generic, a small amount of overhead is added
      * during type conversion.
      */
    def norm(implicit num: Numeric[A]) = {
      import num._
      Math.sqrt(normSquared.toDouble())
    }

    /** Returns the sum of two vectors. */
    def +(y: Seq[A])(implicit num: Numeric[A]) = {
      import num._
      x.zip(y) map { case (xi, yj) => xi + yj }
    }

    /** Returns the difference between two vectors. */
    def -(y: Seq[A])(implicit num: Numeric[A]) = {
      import num._
      x.zip(y) map { case (xi, yj) => xi - yj }
    }

    /** Returns the element-wise product of two vectors. */
    def *(y: Seq[A])(implicit num: Numeric[A]) = {
      import num._
      x.zip(y) map { case (xi, yj) => xi * yj }
    }

    /** Returns the scalar product. Note that the scalar must appear on the
      * right-hand side, e.g. x * 5.
      */
    def *(y: A)(implicit num: Numeric[A]) = {
      import num._
      x map { case (xi) => xi * y }
    }
  }

  /** Provides some statistical functions for sequences.
    *
    * Value class -- doesn't cause overhead.
    */
  implicit class StatOps[A](val x: Seq[A]) extends AnyVal {
    /** Returns the variance. */
    def variance(implicit num: Numeric[A]): Double = {
      import num._

      val mean = x.sum.toDouble / x.size
      val z = x.view.map { _.toDouble - mean }.map { x => x * x }
      z.sum / (x.size - 1)
    }

    /** Returns the standard deviation. */
    def stdev(implicit num: Numeric[A]): Double = Math.sqrt(variance)
  }
}
package com.su.ca.gui;

/**
 * Entry point when running the application from a JAR that executes the Scala
 * GUI application.
 */
public class Main {
	public static void main(String[] args) {
		GUI.main(args);
	}
}

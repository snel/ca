package com
package su
package ca
package gui

import java.io.File
import java.io.BufferedWriter
import java.io.FileWriter
import java.awt.BorderLayout
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Dimension
import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import java.awt.GridLayout
import java.awt.event.KeyEvent
import java.awt.Color
import java.awt.Toolkit
import java.awt.Component
import java.awt.Graphics
import java.awt.Container
import java.awt.Insets
import javax.swing.JMenuItem
import javax.swing.JFileChooser
import javax.swing.JSpinner
import javax.swing.JLabel
import javax.swing.JButton
import javax.swing.event.ChangeEvent
import javax.swing.AbstractButton
import javax.swing.JOptionPane
import javax.swing.event.ChangeListener
import javax.swing.UIManager
import javax.swing.JComponent
import javax.swing.SwingUtilities
import javax.swing.BorderFactory
import javax.swing.JMenuBar
import javax.swing.JMenu
import javax.swing.border.EmptyBorder
import javax.swing.AbstractAction
import javax.swing.KeyStroke
import javax.swing.JComboBox
import javax.swing.ComboBoxModel
import javax.swing.ListCellRenderer
import javax.swing.JList
import javax.swing.SpinnerNumberModel
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.SpringLayout
import java.awt.event.ComponentListener
import java.awt.event.ComponentEvent
import javax.swing.JCheckBoxMenuItem
import javax.swing.filechooser.FileNameExtensionFilter
import java.nio.file.Paths
import java.util.concurrent.atomic.AtomicBoolean
import java.io.FileNotFoundException
import java.awt.event.FocusListener
import java.awt.event.FocusEvent
import java.awt.image.BufferedImage
import javax.swing.ImageIcon
import javax.imageio.ImageIO
import java.awt.Image
import com.su.ca.Lattice

/** GUI for the CA project.
  *
  * @note  In Java 7, the native look and feel implementation contains a bug, causing the exception
  * "java.lang.IllegalArgumentException: Comparison method violates its general contract!"
  * This happens when the grid is large, busy animating and the spinner buttons are activated.
  * The bug appears to be fixed in Java 8.
  */
import javax.swing.JSlider
import scala.collection.mutable.ArrayBuffer
import java.awt.GridBagLayoutInfo
import javax.swing.ImageIcon
import java.awt.image.BufferedImage
import java.awt.Image
import javax.imageio.ImageIO
import javax.swing.AbstractSpinnerModel

/** GUI for the CA project.
  *
  * @note  In Java 7, the native look and feel implementation contains a bug, causing the exception
  * "java.lang.IllegalArgumentException: Comparison method violates its general contract!"
  * This happens when the grid is large, busy animating and the spinner buttons are activated.
  * The bug appears to be fixed in Java 8.
  */

object GUI extends App {
  /** True iff a rule is being applied to the lattice.
    *
    * @note Use setRunning(value) to update its value.
    */
  // Note: volatile != atomic. Volatile guarantees read safety, but atomic also guarantees write concurrent safety. 
  var running = new AtomicBoolean(false)

  var delay = 1000
  var maxDim = 100
  var minDim = 2
  var defaultSize = 5
  var aspectRatioLocked = true

  /** List of functions that are called whenever the playback state changes.
    *
    * The functions will be called from the event dispatch thread.
    */
  val playbackListeners = scala.collection.mutable.Buffer[() => Any]()

  var grid: Grid[_] = {
    if (args.length == 1) {
      Try(Lattice.boolFromFile(args(0))) match {
        case Some(p) => BooleanGrid(p)
        case None => makeGrid(defaultSize, defaultSize)
      }
    } else {
      makeGrid(defaultSize, defaultSize)
    }
  }

  val historyModel = new AbstractSpinnerModel {
    private def t = grid.history.t
    private var x = t
    override def getValue = t.asInstanceOf[Object]
    override def getPreviousValue = (t - 1).asInstanceOf[Object]
    override def getNextValue = (t + 1).asInstanceOf[Object]
    override def setValue(o: Object): Unit = GUI.synchronized {
      val i = o.asInstanceOf[Int]
      if (i != t && i >= 0 && i < grid.history.items.size) {
        grid.history.select(i)
        grid.refresh()
      }
      if (x != t) {
        fireStateChanged()
        x = t
      }
    }
  }

  def historyUpdated(): Unit = Grid.synchronized {
    historyModel.setValue(grid.history.t.asInstanceOf[Object])
  }

  val gridPanel = new JPanel

  fromSwingThread { updateGridPanel(grid) }
  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)

  val frame = new JFrame("Conway's Game of Life") {
    fromSwingThread {
      setSize(1000, 800)
      setLayout(new BorderLayout)
      setJMenuBar(makeMenu)

      add(makePlaybackPanel, BorderLayout.SOUTH)
      add(new JPanel { add(gridPanel) }, BorderLayout.CENTER)

      addComponentListener(new ComponentListener {
        override def componentResized(e: ComponentEvent): Unit = fromSwingThread { gridResized() }
        override def componentMoved(e: ComponentEvent) {}
        override def componentShown(e: ComponentEvent) {}
        override def componentHidden(e: ComponentEvent) {}
      })

      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
      setLocationRelativeTo(null)
      setVisible(true)
    }
  }

  /** Updates the grid panel when it resizes.
    *
    * Provides support for aspect ratio locking.
    *
    * Assumed to be run from the event dispatch thread.
    */
  def gridResized(): Unit = {
    //    require(SwingUtilities.isEventDispatchThread())

    val parent = gridPanel.getParent
    val sz = parent.getSize()
    if (aspectRatioLocked) {
      val d = Math.min(sz.height / grid.lattice.dims(0),
        sz.width / grid.lattice.dims(1))

      val dh = grid.lattice.dims(0) * d
      val dw = grid.lattice.dims(1) * d

      sz.setSize(dw, dh)
    }
    gridPanel.setPreferredSize(sz)
    parent.revalidate()
    parent.repaint()
  }

  /** Returns a panel for controlling the simulation.
    *
    * Assumed to be run from the event dispatch thread.
    */
  private def makePlaybackPanel: JPanel = new JPanel() {
    //    require(SwingUtilities.isEventDispatchThread())

    setPreferredSize(new Dimension(200, 40))

    add(new JLabel("Delay (ms):"))
    val speedSpinnerModel = new SpinnerNumberModel(delay, 1, 10000, 100)
    add(new JSpinner(speedSpinnerModel) {
      this.setAction(() => delay = speedSpinnerModel.getNumber.intValue())
    })

    add(new JButton {
      def update(): Unit = {
        if (running.get) {
          setAction("Pause", () => pause())
        } else {
          setAction("Play", () => fromWorkerThread { play() })
        }
      }

      setPreferredSize(new Dimension(80, 30))
      playbackListeners += (() => update())
      update()
    })

    add(new JButton {
      def update(): Unit = fromSwingThread { setEnabled(!running.get) }
      setPreferredSize(new Dimension(80, 30))
      setAction("Step", () => fromWorkerThread { step() })
      playbackListeners += (() => update())
      update()
    })

    add(new JLabel("t:"))
    add(new JSpinner(historyModel) {
      setPreferredSize(new Dimension(70, 20))
    })
  }

  /** Returns a boolean grid of the given size. */
  private def makeGrid(h: Int, w: Int): Grid[_] =
    BooleanGrid(Lattice(false)(IndexedSeq(h, w)))

  /** Replaces the current grid object with the given one.
    *
    * Assumed to be run from the event dispatch thread.
    */
  def updateGridPanel(grid: Grid[_]): Unit = {
    //    require(SwingUtilities.isEventDispatchThread())

    GUI.synchronized {
      setRunning(false)
      this.grid = grid
      gridPanel.removeAll()

      val dims = grid.lattice.dims
      gridPanel.setLayout(new GridLayout(dims(0), dims(1)) {
        setHgap(1)
        setVgap(1)
      })
      grid.lattice.cells.foreach { x => gridPanel.add(grid.cells(x)) }
      gridPanel.revalidate()
      gridPanel.repaint()

      historyUpdated()
    }
  }

  /** Returns a menubar.
    *
    * Assumed to be run from the event dispatch thread.
    */
  private def makeMenu: JMenuBar = new JMenuBar() {
    add(new JMenu("File") {
      setMnemonic(KeyEvent.VK_F)
      getAccessibleContext().setAccessibleDescription("Main menu")

      add(new JMenuItem("New lattice") {
        setMnemonic(KeyEvent.VK_N)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))
        setAction { () =>
          val hSpinnerModel = new SpinnerNumberModel(grid.lattice.dims(0), minDim, maxDim, 1)
          val wSpinnerModel = new SpinnerNumberModel(grid.lattice.dims(1), minDim, maxDim, 1)
          val panel = new JPanel {
            setLayout(new GridLayout(2, 2))

            add(new JLabel("Width:"))
            add(new JSpinner(wSpinnerModel))

            add(new JLabel("Height:"))
            add(new JSpinner(hSpinnerModel))
          }

          pause()
          var ret = JOptionPane.showConfirmDialog(
            frame,
            panel,
            "New lattice",
            JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.PLAIN_MESSAGE)

          if (ret == JOptionPane.OK_OPTION) {
            var h = hSpinnerModel.getNumber.intValue()
            var w = wSpinnerModel.getNumber.intValue()
            updateGridPanel(makeGrid(h, w))
            gridResized()
          }
        }
      })

      add(new JMenuItem("Select rule") {
        setMnemonic(KeyEvent.VK_R)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))

        setAction { () =>
          val combo = new JComboBox(grid.rulePalette.keySet.toArray)
          combo.setSelectedItem(grid.selectedRule)
          val panel = new JPanel {
            setLayout(new GridLayout(2, 2))
            add(new JLabel("Rule:"))
            add(combo)
          }

          pause()
          var ret = JOptionPane.showConfirmDialog(
            frame,
            panel,
            "Select rule",
            JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.PLAIN_MESSAGE)

          if (ret == JOptionPane.OK_OPTION) {
            grid.setRule(combo.getSelectedItem.asInstanceOf[String])
          }
        }
      })

      add(new JMenuItem("Open lattice") {
        setMnemonic(KeyEvent.VK_O)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))
        setAction { () =>
          pause()
          val chooser = new JFileChooser("./SavedFiles")
          chooser.setFileFilter(new FileNameExtensionFilter("Lattice", "txt"))

          chooser.showOpenDialog(frame) match {
            case JFileChooser.APPROVE_OPTION =>
              val path = chooser.getSelectedFile.getAbsolutePath
              fromWorkerThread {
                val lattice = Lattice.boolFromFile(path)
                fromSwingThread {
                  updateGridPanel(BooleanGrid(lattice))
                  gridResized()
                }
              }
            case _ =>
          }
        }
      })

      add(new JMenuItem("Save lattice") {
        setMnemonic(KeyEvent.VK_S)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))
        setAction { () =>
          pause()
          selectFile() match {
            case Some(file) => fromWorkerThread {
              grid.lattice.synchronized { saveLattice(file) }
            }
            case None =>
          }
        }
      })

      add(new JMenuItem("Screenshot") {
        setMnemonic(KeyEvent.VK_P)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))
        addActionListener(
          new ActionListener() {
            def actionPerformed(e: ActionEvent) {
              val img = screenshot()
              // Dudes, this does nothing.
              //              JOptionPane.showMessageDialog(
              //                null,
              //                new JLabel(
              //                  new ImageIcon(
              //                    img.getScaledInstance(
              //                      img.getWidth(null) / 2,
              //                      img.getHeight(null) / 2,
              //                      Image.SCALE_SMOOTH))));
              try {
                ImageIO.write(
                  img,
                  "png",
                  new File("screenshot.png"));
              } catch {
                case _: Exception =>
              }
            }
          })
      })

      add(new JMenuItem("Quit") {
        setMnemonic(KeyEvent.VK_Q)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))
        setAction { () => System.exit(0) }
      })
    })

    add(new JMenu("View") { menu =>
      setMnemonic(KeyEvent.VK_V)
      getAccessibleContext().setAccessibleDescription("Modify visual appearance")

      add(new JCheckBoxMenuItem {
        setSelected(aspectRatioLocked)
        setAction("Lock aspect ratio", () => {
          aspectRatioLocked = !aspectRatioLocked
          fromSwingThread { gridResized() }
        })
      })

      add(new JMenuItem("Screenshot") {
        setMnemonic(KeyEvent.VK_P)
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, Toolkit.getDefaultToolkit.getMenuShortcutKeyMask))
        setAction { () =>
          val img = screenshot()
          Try {
            ImageIO.write(
              img,
              "png",
              new File("screenshot.png"))
          }
          ()
        }
      })
    })

    def screenshot(): BufferedImage = {
      var image = new BufferedImage(
        frame.getWidth,
        frame.getHeight,
        BufferedImage.TYPE_INT_RGB);
      // call the Component's paint method, using
      // the Graphics object of the image.
      frame.paint(image.getGraphics) // TODO: use gridPanel.paint instead
      image
    }

    def selectFile(rootDir: String = "./SavedFiles"): Option[File] = {
      val chooser = new JFileChooser(new File(rootDir))
      chooser.setFileFilter(new FileNameExtensionFilter("Lattice", "txt"))

      chooser.showSaveDialog(frame) match {
        case JFileChooser.APPROVE_OPTION =>
          var file = chooser.getSelectedFile

          // Adds a .txt file extension if there was none
          val q = ".*[.](.*)".r.findFirstIn(file.getPath) match {
            case Some(p) =>
            case _ => file = new File(file.getPath + ".txt")
          }

          if (file.exists()) {
            JOptionPane.showConfirmDialog(frame, "That file already exists, overwrite?",
              "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) match {
                case JOptionPane.CANCEL_OPTION => return None
                case JOptionPane.NO_OPTION => selectFile()
                case _ =>
              }
          }
          Some(file)
        case _ => None
      }
    }
  }

  def screenshot(): BufferedImage = {
    var image = new BufferedImage(
      frame.getWidth(),
      frame.getHeight(),
      BufferedImage.TYPE_INT_RGB)
    frame.paint(image.getGraphics()) // alternately use .printAll(..)
    image
  }

  // These functions have synchronization from inside the worker thread, and 
  // not the other way around because otherwise the state might have changed
  // by the time the worker starts.

  /** Steps playback -- applies the rule once.
    *
    * Should not be called from the GUI thread.
    */
  def step(): Unit = {
    if (!running.get) {
      GUI.synchronized {
        setRunning(true)
        grid.takeStep()
        setRunning(false)
        historyUpdated()
      }
    }
  }

  /** Starts playback -- applies the rule continuously.
    *
    * Should not be called from the GUI thread.
    */
  def play(): Unit = {
    if (!running.get) {
      setRunning(true)
      while (running.get) {
        GUI.synchronized {
          grid.takeStep()
          GUI.wait(delay) // wait, unlike Thread.sleep, releases the lock and allows interruption.
          historyUpdated()
        }
      }
    }
  }

  /** Stops playback -- stops applying the rule.
    *
    * @note Won't quit halfway through computation or leave it in an inconsistent state.
    */
  def pause(): Unit = setRunning(false)

  private def setRunning(value: Boolean): Unit = {
    running.set(value)
    fromSwingThread {
      playbackListeners.foreach { _() }
    }
  }

  /** Saves the lattice to the file. */
  def saveLattice(file: File): Unit = {
    import Lattice._

    val w = new BufferedWriter(new FileWriter(file))
    try {
      w.write(s"${grid.lattice.dims(1)} ${grid.lattice.dims(0)}\r\n")
      w.write(grid.lattice.tabulate(rowSep = "\r\n"))
    } finally {
      w.close()
    }
  }

  /** Saves the entire set of lattices from the start of a test to where the
    * test ended
    */
  def saveLatticeHistory(file: File): Unit = {
    import Lattice._

    val w = new BufferedWriter(new FileWriter(file))
    try {
      w.write(s"${grid.lattice.dims(1)} ${grid.lattice.dims(0)}\r\n")
      grid.history.items.foreach { x => w.write(x.toString() + "\r\n") }
    } finally {
      w.close()
    }
  }

  /** Tries to compute the value of f, or shows an error dialog otherwise. */
  def Try[A](f: => A): Option[A] = {
    try {
      Some(f)
    } catch {
      case e: Exception =>
        e.printStackTrace()
        JOptionPane.showMessageDialog(frame, e, "Error", JOptionPane.ERROR_MESSAGE)
        None
    }
  }

  /** Adds a utility method to buttons.
    *
    * Catches exceptions with the GUI error handler.
    */
  implicit class ButtonSugar(val button: AbstractButton) extends AnyVal {
    def setAction(caption: String, f: () => Unit) =
      button.setAction(new AbstractAction(caption) {
        def actionPerformed(e: ActionEvent): Unit = Try(f())
      })
  }

  /** Adds a utility method to menu items.
    *
    * Catches exceptions with the GUI error handler.
    */
  implicit class MenuItemSugar(val item: JMenuItem) extends AnyVal {
    def setAction(f: () => Unit) =
      item.addActionListener(new ActionListener() {
        def actionPerformed(e: ActionEvent): Unit = Try(f())
      })
  }

  /** Adds a utility method to spinners.
    *
    * Catches exceptions with the GUI error handler.
    */
  implicit class SpinnerSugar(val spinner: JSpinner) extends AnyVal {
    def setAction(f: () => Unit) =
      spinner.addChangeListener(new ChangeListener() {
        override def stateChanged(e: ChangeEvent): Unit = Try(f())
      })
  }

  /** Executes the action from the Swing event dispatch thread.
    *
    * Catches exceptions with the GUI error handler.
    */
  def fromSwingThread(action: => Any) =
    SwingUtilities.invokeLater(new Runnable {
      override def run() = Try(action)
    })

  /** Executes the action from a new worker thread.
    *
    * Catches exceptions with the GUI error handler.
    */
  def fromWorkerThread(action: => Any) =
    new Thread {
      override def run() = Try(action)
    }.start()
}
package com
package su
package ca
package gui

import javax.swing.BorderFactory
import javax.swing.Icon
import javax.swing.JPanel
import javax.swing.JLabel
import javax.swing.SwingConstants
import javax.swing.SwingUtilities
import javax.swing.JOptionPane
import javax.swing.JFrame
import java.awt.BasicStroke
import java.awt.event.MouseEvent
import java.awt.event.MouseAdapter
import java.awt.Color
import java.awt.Component
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.event.MouseMotionListener
import scala.collection.mutable.ArrayBuffer
import prisoners.Prisoner
import migration.MigrationModel
import java.awt.Font

/** Grid of editable cells. */
abstract class Grid[A](
    val lattice: BufferedLattice[A],
    val rulePalette: Map[String, Rule[A]],
    defaultRule: String,
    caption: Seq[Int] => String = x => "",
    circular: Seq[Int] => Boolean = x => true,
    connections: Seq[Int] => Array[Boolean] = x => Array(false, false, false, false)) {

  protected var rule: Rule[A] = rulePalette(defaultRule)
  protected var _selectedRule = defaultRule
  /** Returns the name of the selected rule. */
  def selectedRule = _selectedRule
  var draggedOver: JLabel = null
  val font = new Font(Font.SANS_SERIF, 0, 8)
  /** Maps coordinates to selectable cells. */
  val cells: Map[Seq[Int], JLabel] = lattice.cells.map { x => x -> makeLabel(x) }.toMap
  private val coords = cells.map { case (k, v) => v -> k }
  val history: History[A] = History[A](lattice)

  GUI.fromSwingThread { refresh() }

  /** Icon that draws special cell contents.
    */
  private class CellIcon extends Icon {
    private var width = 0
    private var height = 0
    private var color = Color.BLACK
    private var filled = false
    private var links = Seq(false, false, false, false)

    /** Constructs a cell icon.
      *
      * @param w Width of cell.
      * @param h Height of cell.
      * @param color Colour of cell.
      * @param filled Whether the cell is filled or not (if not, then it is
      * circular.
      * @param links Whether it is linked to the cells to its top, left, bottom
      * or right, respectively.
      */
    def this(w: Int, h: Int, color: Color, filled: Boolean, links: Seq[Boolean]) = {
      this()
      this.width = w
      this.height = h
      this.color = color
      this.filled = filled
      this.links = links
    }

    /** Draw the cell contents.
      */
    override def paintIcon(c: Component, g: Graphics, x: Int, y: Int) = {
      val cx = width / 2
      val cy = height / 2
      val temp = g.getColor()
      g.setColor(color)
      val g2 = g.asInstanceOf[Graphics2D]
      g2.setStroke(new BasicStroke(4))
      if (this.filled) {
        g.fillRect(0, 0, getIconWidth(), getIconHeight())
      } else {
        g.fillOval(0, 0, getIconWidth(), getIconHeight())
      }
      g.setColor(Color.WHITE)
      if (this.links(0)) {
        g.drawLine(cx, cy, cx, 0)
      }
      if (this.links(1)) {
        g.drawLine(cx, cy, 0, cy)
      }
      if (this.links(2)) {
        g.drawLine(cx, cy, cx, this.height)
      }
      if (this.links(3)) {
        g.drawLine(cx, cy, this.width, cy)
      }
      g.setColor(temp)
    }

    override def getIconWidth(): Int = {
      width
    }

    override def getIconHeight(): Int = {
      height
    }
  }

  def makeLabel(x: Seq[Int]) = new JLabel(caption(x), SwingConstants.CENTER) { label =>
    label.setFont(font)
    label.setForeground(Color.BLACK)
    setOpaque(true)
    val m = new MouseAdapter() {
      override def mousePressed(e: MouseEvent): Unit = {
        select(x)
        GUI.fromSwingThread { refresh(x) }
      }

      // The following two methods allow the user to edit multiple cells simply by dragging over them.

      override def mouseReleased(e: MouseEvent): Unit = {
        draggedOver = null
      }

      override def mouseDragged(e: MouseEvent): Unit = {
        val p0 = GUI.gridPanel.getLocationOnScreen
        val p1 = e.getLocationOnScreen
        val component = GUI.gridPanel.getComponentAt(p1.x - p0.x, p1.y - p0.y)
        if (label == component || component == draggedOver || !component.isInstanceOf[JLabel]) return

        val yLabel = component.asInstanceOf[JLabel]
        coords.get(yLabel) match {
          case Some(y) =>
            draggedOver = yLabel
            select(y)
            GUI.fromSwingThread { refresh(y) }
          case None =>
        }
      }
    }
    addMouseListener(m)
    addMouseMotionListener(m)
  }

  /** Refreshes the cell label at x.
    *
    * Assumed to run from the event dispatch thread.
    */
  protected def refresh(x: Seq[Int]): Unit = {
    //    require(SwingUtilities.isEventDispatchThread())

    val y = lattice.synchronized { lattice(x) }
    val label = cells(x)

    label.setBackground(Color.WHITE)
    label.setIcon(new CellIcon(label.getWidth, label.getHeight, colour(y), circular(x), connections(x)))
    label.setText(caption(x))

    label.revalidate()
  }

  /** Updates the grid with values from the lattice. */
  def refresh(): Unit = {
    lattice.cells.foreach { refresh(_) }
  }

  /** Applies the rule once and updates the grid panel. */
  def takeStep(): Unit = lattice.synchronized {
    history.insert()
    rule.apply(lattice)
    lattice.apply()
    GUI.fromSwingThread { refresh() }
  }

  /** Selects the rule to be applied to the lattice. */
  def setRule(selection: String) {
    rule = rulePalette(selection)
  }

  /** Updates the cell at x with a new value from user input. */
  def select(x: Seq[Int]): A

  /** Returns the colour associated with the given value. */
  def colour(value: A): Color
}

/** Companion for Grid. */
object Grid {
  /** Returns a general-purpose grid.
    *
    * @param lattice the underlying lattice
    * @param f selects a new value for a given cell.
    * 	(The function does not need to synchronize itself on the lattice, since we do that here.)
    * @param rulePalette a selection of rules compatible with the lattice
    * @param colourMap maps values to cell colours
    */

  def apply[A](
    lattice: BufferedLattice[A],
    f: Seq[Int] => A,
    rules: Map[String, Rule[A]],
    defaultRule: String,
    colourMap: A => Color,
    caption: Seq[Int] => String = x => "",
    circular: Seq[Int] => Boolean = x => true,
    connections: Seq[Int] => Array[Boolean] = x => Array(false, false, false, false)) =
    new Grid(lattice, rules, defaultRule, caption, circular, connections) {
      override def colour(value: A) = colourMap(value)
      override def select(x: Seq[Int]): A = lattice.synchronized {
        val y = f(x)
        lattice(x) = y
        lattice.applyAt(x)
        y
      }
    }
}

/** Factory for boolean grids. */
object BooleanGrid {
  /** Returns a boolean grid. */
  def apply(lattice: BufferedLattice[Boolean]) = {
    def colourMap(value: Boolean) = if (value) Color.GREEN else Color.WHITE
    val rulePalette = Map[String, Rule[Boolean]](
      "Conway, periodic" -> ConwayBoolean(Moore(1, periodic = true)),
      "Conway, null" -> ConwayBoolean(Moore(1, periodic = false)))
    Grid(lattice, x => !lattice(x), rulePalette, "Conway, periodic", colourMap)
  }
}

/** Factory for integer grids. */
object IntGrid {
  /** Returns a integer grid.
    *
    * @todo complete this for use with the main project
    */
  def apply(lattice: BufferedLattice[Int], rule: Rule[Int]) = {
    def colourMap(value: Int) = if (value > 0) Color.GREEN else Color.WHITE
    val rulePalette = Map[String, Rule[Int]](
      "rule" -> rule)
    Grid(lattice, x => if (lattice(x) > 0) 0 else 1, rulePalette, "rule", colourMap)
  }
}

/** Factory for prisoner grid */
object PrisonerGrid {
  /** Returns a prisoner grid */
  def apply(lattice: BufferedLattice[Prisoner], rule: Rule[Prisoner]) = {
    def colourMap(value: Prisoner) = if (value.state > 0) Color.GREEN else Color.WHITE
    val rulePalette = Map[String, Rule[Prisoner]]("rule" -> rule)
    //    def caption(x: Seq[Int]) = {
    //      val v = lattice(x)
    //      "%.1f | %.1f".format(v.defacc, v.coopacc)
    //    }
    def caption(x: Seq[Int]) = ""

    Grid(lattice, x => {
      val v = lattice(x)
      if (v.state > 0) {
        new Prisoner(0, v.defacc, v.coopacc)
      } else {
        new Prisoner(1, v.defacc, v.coopacc)
      }
    }, rulePalette, "rule", colourMap, caption)
  }
}

/** Factory for migration grid */
object MigrationGrid {

  /** Returns a migration grid */
  def apply(lattice: BufferedLattice[MigrationModel], rule: Rule[MigrationModel]) = {
    // Colours used in website article.
    val colors: Array[Color] = Array(Color.white, new Color(15, 8, 150), new Color(2, 112, 235), new Color(0, 117, 144),
      new Color(7, 129, 18), new Color(93, 154, 1), new Color(255, 248, 12), new Color(255, 181, 0),
      new Color(248, 104, 5), new Color(208, 0, 4))
    // Blue range.
    //    val colors: Array[Color] = Array(Color.white, new Color(3, 30, 80), new Color(8, 48, 107), new Color(8, 81, 156), new Color(33, 113, 181), new Color(66, 146, 198),
    //      new Color(107, 174, 214), new Color(158, 202, 225), new Color(198, 219, 239), new Color(222, 235, 247))
    def colourMap(value: MigrationModel) = colors(value.state)
    val rulePalette = Map[String, Rule[MigrationModel]]("rule" -> rule)
    def caption(x: Seq[Int]) = {
      // TODO decide what caption should be.
      val v = lattice(x)
      if (v.satisfied) "S" else "NS"
    }
    def rel(x: Seq[Int]) = {
      val v = lattice(x)
      v.rel
    }.toArray
    def circular(x: Seq[Int]) = {
      val v = lattice(x)
      v.satisfied
    }

    Grid(lattice, x => {
      val v = lattice(x)
      new MigrationModel(v.state)
    }, rulePalette, "rule", colourMap, caption, circular, rel)
  }
}
/*


 * CA project
 * @author S. E. A. Nel
 * @author D. D. Malan
 */

package com.su.ca

import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
import java.util.HashMap
import scala.collection.parallel.mutable.ParArray
import scala.collection.mutable.BitSet
import java.io.DataInputStream
import java.io.File
import scala.io.Source
import javax.swing.JSlider
import scala.util.matching.Regex

/** Multi-dimensional lattice of cellular automaton states.
  *
  * All coordinates are assumed to be non-negative.
  *
  * @note Suppose we have a lattice `lat` and want to get a value at `x = (x0, y0)`.
  * We can use either `lat(x0, y0)` or `lat(Seq(x0, y0))`.
  * We can set the value at that position to `z` by using `lat(Seq(x0, x1)) = z`.
  *
  * @tparam A cell state type
  * @see [[MultiArray]]
  *
  * @define applyDescription Returns the state of the cell at the given coordinates.
  * @define optionalApplyDescription Optionally returns the state of the cell at the given coordinates.
  */
abstract class Lattice[A] {
  /** The underlying data type. */
  val dataType: ClassTag[A]
  /** The underlying data consisting of cell states. */
  def data: MultiArray[A]

  /** Returns the shape (dimensions) of this lattice.
    *
    * The lower bounds are assumed to be 0.
    * The numbers returned are upper bounds (non-inclusive).
    */
  def dims: IndexedSeq[Int] = data.dims

  /** $applyDescription */
  def apply(x: Seq[Int]): A
  /** $applyDescription */
  def apply(x0: Int, x: Int*): A = apply(x.+:(x0)) // alias for the method above

  /** $optionalApplyDescription */
  def get(x: Seq[Int]): Option[A]
  /** $optionalApplyDescription */
  def get(x0: Int, x: Int*): Option[A] = get(x.+:(x0)) // alias for the method above

  /** Updates the state of the cell at the given coordinates. */
  def update(x: Seq[Int], newState: A): Unit

  /** Returns an iterator over cell coordinates from the lattice. */
  def cells: Iterator[Seq[Int]] = SeqMath.cartesian(dims.map { 0 until _ }) // Cartesian product of each dimension

  /** Returns a parallel array of iterators over cell coordinates from the lattice.
    *
    * The array is the same size as the first axis of the lattice, and each
    * iterator iterates over cells in that axis.
    */
  // We needed a way to subdivide the space (e.g. by the first dimension) to hand them to threads.
  // The obvious thing would be:
  //     def dividedCells = cells.grouped(dims.head).toSeq.par
  // but in the process it converts the nested iterators to collections.
  // This method avoids that and allows lazy iteration over each division. 
  def dividedCells: ParArray[Iterator[Seq[Int]]] = {
    val x = dims.tail.map { 0 until _ }
    (0 until dims.head).map { d => SeqMath.cartesian(x).map { y => y.+:(d) } }.toParArray
  }

  /** Returns a String representation of the Lattice. */
  override def toString: String = {
    data.array.map { x =>
      if (this.dataType.runtimeClass == classOf[Boolean]) {
        s"${if (x == true) 1 else 0}"
      } else {
        x.toString
      }
    }.mkString(" ")
  }
}

/** Companion for Lattice and BufferedLattice. */
object Lattice {
  /** Creates a buffered lattice with the given dimensions. */
  def apply[A](initialValue: A, dirty: Boolean = false)(dims: IndexedSeq[Int])(implicit classtag: ClassTag[A]): BufferedLattice[A] =
    build({
      val data = MultiArray.fill[A](dims)(initialValue)
      new LatticeImpl[A](data, dims)
    }, dirty)

  /** Creates a buffered boolean lattice with the given dimensions. */
  def boolean(initialValue: Boolean = false, dirty: Boolean = false)(dims: IndexedSeq[Int]): BufferedLattice[Boolean] =
    build({
      val data = MultiArray.fillBoolean(dims)(initialValue)
      new LatticeImpl[Boolean](data, dims)
    }, dirty)

  private def build[A](make: => Lattice[A], dirty: Boolean = false)(implicit classtag: ClassTag[A]) = {
    if (dirty) new BufferedLattice(make, make) with Dirty[A]
    else new BufferedLattice(make, make)
  }

  /** Reads a Boolean lattice of 0's and 1's from a file as specified in the project instructions. */
  def boolFromFile(filename: String): BufferedLattice[Boolean] = {
    val f = scala.io.Source.fromFile(new File(filename))
    try {
      boolFromSource(f)
    } finally {
      f.close()
    }
  }

  /** Reads a Boolean lattice of 0's and 1's from a source. */
  def boolFromSource(f: Source): BufferedLattice[Boolean] = {
    val text = f.getLines()

    var r, c = 0
    try {
      val head = "\\d+".r.findAllIn(text.next())
      val h = head.next().toInt
      val w = head.next().toInt
      val lattice = Lattice[Boolean](false)(IndexedSeq(h, w))

      val elem = "[01]".r
      for (r <- 0 until h) {
        val line = elem.findAllIn(text.next())
        for (c <- 0 until w) {
          lattice(Seq(r, c)) = line.next().toInt == 1
        }
        if (line.hasNext) throw new Exception("unexpected token")
      }
      if (text.hasNext) throw new Exception("unexpected token")

      lattice.apply()
      lattice
    } catch {
      case e: Throwable => throw new Exception(s"bad file format, line ${r + 1} column $c", e)
    } finally {
      f.close()
    }
  }

  /** Reads a Int lattice from a file. */
  def intFromFile(filename: String): BufferedLattice[Int] = {
    val f = scala.io.Source.fromFile(new File(filename))
    try {
      intFromSource(f)
    } finally {
      f.close()
    }
  }

  /** Reads an Int lattice from a source. */
  def intFromSource(source: Source): BufferedLattice[Int] = {
    val text = source.getLines()

    val lattice = readDefinition(source, 0).asInstanceOf[BufferedLattice[Int]]
    readIntData(source, lattice)

    if (text.hasNext) throw new Exception("unexpected token")
    lattice
  }

  def readDefinition[A](f: Source, default: A)(implicit classtag: ClassTag[A]): BufferedLattice[A] = {
    val text = f.getLines()

    val head = "\\d+".r.findAllIn(text.next())
    val h = head.next().toInt
    val w = head.next().toInt

    // XXX: modify file format to specify the type beforehand.
    Lattice[A](default)(IndexedSeq(h, w))
  }

  def readIntData(source: Source, lattice: BufferedLattice[Int]): Unit =
    readData(source, lattice, "\\d+".r, s => s.toInt)

  /** Reads data into the given lattice from a source. */
  def readData[A](source: Source, lattice: BufferedLattice[A], regex: Regex, parse: String => A): Unit = {
    val text = source.getLines()
    var r = 0
    val i = lattice.cells

    while (i.hasNext)
      regex.findAllIn(text.next()).foreach { p =>
        lattice(i.next()) = parse(p)
        r += 1
      }

    lattice.apply()
  }

  // TODO An implementation that stores data on disk, rather than in memory, 
  // might be a good idea so that we can run larger experiments. We could cache
  // some of the values to make it faster.

  /** Lattice implementation
    *
    * @param _data storage for cell states
    * @param _dims array dimensions
    * @tparam A cell state type
    */
  private class LatticeImpl[A](_data: MultiArray[A], _dims: Seq[Int])(implicit val dataType: ClassTag[A]) extends Lattice[A] {
    override def data = _data
    override def apply(x: Seq[Int]) = _data(x)
    override def get(x: Seq[Int]) = _data.get(x)
    override def update(x: Seq[Int], newState: A) = _data(x) = newState
  }

  /** Implicitly wraps lattices to provide the tabulate method.
    * Of course this won't work with lattices that are not 2D.
    */
  implicit class Lattice2D(val lattice: Lattice[_]) extends AnyVal {
    /** Returns the 2D lattice in tabular form.
      * @param width The width of each column
      */
    def tabulate(width: Int = 2, colSep: String = " ", rowSep: String = "\n"): String = {
      (0 until lattice.dims(0)).map { i =>
        (0 until lattice.dims(1)).map { j =>
          if (lattice.dataType.runtimeClass == classOf[Boolean])
            s"${if (lattice(i, j) == true) 1 else 0}"
          else
            s"${lattice(i, j)}".formatted(s"%${width}s")
        }.mkString(colSep)
      }.mkString(rowSep)
    }

    /** Returns the 2D lattice in tabular form. Each column is assumed to be 3 characters wide. */
    def tabulate: String = tabulate(2)
  }
}

/** Wraps a lattice and its buffer.
  *
  * When the update method is called, the input lattice remains unmodified,
  * while the buffer is modified instead. This way, we can consider the lattice
  * to be immutable while applying a rule to it.
  * When the rule has finished, call the apply() method to save the changes.
  *
  * @tparam A cell state type
  */
class BufferedLattice[A](protected val lattice: Lattice[A], protected val buffer: Lattice[A])(implicit val dataType: ClassTag[A]) extends Lattice[A] {
  override def data = lattice.data
  override def dims = lattice.dims
  override def apply(x: Seq[Int]) = lattice.apply(x)
  override def get(x: Seq[Int]) = lattice.get(x)
  override def update(x: Seq[Int], newState: A) = buffer.update(x, newState)

  /** Saves the changes made to the buffer to the lattice. */
  // Note that simply flipping `lattice` with `buffer` instead of copying wouldn't 
  // work when two or more rules are applied.
  // That could be worked around by explicitly updating every cell whenever a
  // change is made to even one -- but mistakes can lead to subtle bugs.
  // This step is very fast (200ms) anyway, so it's a reasonable trade-off to rather 
  // write some values 'twice' but be safe.
  def apply(): Unit = {
    lattice.cells foreach { applyAt(_) }
  }

  /** Updates the lattice with the value in the buffer at x. */
  def applyAt(x: Seq[Int]): Unit = { lattice(x) = buffer(x) }
}

/** Improves apply() performance for BufferedLattice when many states don't change.
  *
  * Returns immediately when no states changed.
  */
trait Dirty[A] extends BufferedLattice[A] {
  // Bit set representing cells which have changed.
  val dirty = MultiArray.fillBoolean(dims)(false)
  val dirtyBits = dirty.array.asInstanceOf[BitArray].bits

  override def update(x: Seq[Int], newState: A) = {
    if (apply(x) != newState) {
      super.update(x, newState)
      dirty(x) = true
    }
  }

  override def apply(): Unit = {
    if (dirtyBits.isEmpty) return
    lattice.cells.foreach { x =>
      if (dirty(x)) lattice(x) = buffer(x)
    }
    dirtyBits.clear()
  }
}
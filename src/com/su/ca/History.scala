package com.su.ca

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import java.io.File

object History {
  def apply[A](lattice: BufferedLattice[A]) = new History[A](lattice)

  // TODO: replace usages of Lattice.fromFile with this.
  /** Reads an experiment history from a file. */
  def intHistoryFromFile(filename: String): (BufferedLattice[Int], History[Int]) = {
    val source = scala.io.Source.fromFile(new File(filename))
    try {
      val lattice = Lattice.readDefinition[Int](source, 0).asInstanceOf[BufferedLattice[Int]] //XXX
      val history = new History(lattice)
      while (source.hasNext) {
        Lattice.readIntData(source, lattice)
        history.insert()
      }
      (lattice, new History(lattice))
    } finally {
      source.close()
    }
  }
}

// TODO: set limit on this using `capacity`. Then we also need another variable to store the time step name
class History[A](lattice: BufferedLattice[A], capacity: Int = 5) {
  val items = ArrayBuffer[Seq[A]]()
  var t = 0

  def insert(): Unit = {
    if (items.size > t)
      items.remove(t, items.size - t) // removes future items if we selected an older time step

    items.append(lattice.data.array.clone)
    t += 1
  }

  def select(i: Int): Unit = {
    if (i == t) return
    if (i < 0 || i > items.size) throw new IllegalArgumentException

    // adds the last item since it's still new
    if (i < t && t == items.size) insert()

    t = i
    val data = scala.collection.mutable.IndexedSeq.empty ++ items(i)
    val h = new MultiArray[A](data, lattice.dims)
    lattice.cells.foreach { x => lattice(x) = h(x) }
    lattice.apply()
  }
}
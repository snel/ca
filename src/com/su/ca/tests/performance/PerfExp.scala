/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca
package tests
package performance

import SeqMath._

/** Performance experiment, run `n` times. */
abstract class PerfExp[A](title: String, n: Int = 5) {
  def lattice: BufferedLattice[A]

  println("-" * 80)
  println(title)

  def run(action: => Any)(reset: => Any) {
    val x = (0 until n).map { i =>
      reset
      val t = Timed(action)
      println(s"$i: $t")
      t
    }

    val t = x.sum
    println(s"total time elapsed: $t ms")
    println(f"mean=${t / n}%.0f ms, stdev=${x.stdev}%.0f ms")
  }

  /** Sets the value of cells to `value0` with probability `f` and `value1` with probability `1 - f`  */
  def edit(f: Double, value0: A, value1: A) = {
    val p = 1 - f
    lattice.cells.foreach { x =>
      lattice(x) = if (Math.random() >= p) value0 else value1
    }
  }
}

class Next42Boolean(neighbourhood: Neighbourhood) extends Rule[Boolean] {
  /** Sets all states to true, if any true values are in the neighbourhood. */
  override def apply(lattice: BufferedLattice[Boolean]) = {
    lattice.dividedCells.foreach {
      _.foreach { x =>
        if (pred(x, lattice)) lattice(x) = true
      }
    }
  }

  def pred(x: Seq[Int], lattice: BufferedLattice[Boolean]) =
    neighbourhood(x, lattice).exists { y => x != y && lattice(y) }
}

object Next42Boolean {
  def apply(neighbourhood: Neighbourhood) = new Next42Boolean(neighbourhood)
}
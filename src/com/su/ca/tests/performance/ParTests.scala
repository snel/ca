/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca
package tests
package performance

import java.util.Date

/** Displays performance statistics comparing serial with parallel performance.
  *
  * @todo No significant improvement for parallel. Is this because of the Boolean lattice?
  */
object ParExperiments extends App {
  println(getClass.getSimpleName)
  println(new Date())

  for {
    dims <- Seq(
      IndexedSeq(50, 50, 50), // warmup
//      IndexedSeq(16777216),
//      IndexedSeq(4096, 4096)
      IndexedSeq(256, 256, 256)
      )
    title <- Seq(
      "serial",
      "parallel")
  } new PerfExp[Boolean](s"$title, ${dims.mkString("x")}", n = 10) {
    val lattice = Lattice[Boolean](false)(dims)

    val rule = title match {
      case "parallel" =>
        new Rule[Boolean] {
          override def apply(lattice: BufferedLattice[Boolean]) = {
            lattice.dividedCells.foreach {
              _.foreach { x =>
                lattice(x) = true
              }
            }
          }
        }
      case "serial" =>
        new Rule[Boolean] {
          override def apply(lattice: BufferedLattice[Boolean]) = {
            lattice.cells.foreach { x =>
              lattice(x) = true
            }
          }
        }
    }

    run { rule.apply(lattice) } { edit(.5, true, false) }
  }
}
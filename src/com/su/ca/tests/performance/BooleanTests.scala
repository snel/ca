/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca
package tests
package performance

import java.util.Date

/** Displays performance statistics comparing boolean and generic neighbourhoods.
  *
  * @note  We can't show a memory use comparison, but the boolean lattice
  * can be much larger than the generic one before overrunning the heap.
  * For example, a 1000^3 array of 1 byte simple booleans requires 480 MB of
  * heap space, but a 1000^3 bit set requires only about 120 MB.
  */
object BooleanTests extends App {
  println(getClass.getSimpleName)
  println(new Date())

  val f = .5
  val neighbourhood = Moore(2)
  for {
    title <- Seq("Boolean", "Generic")
    dims <- Seq(IndexedSeq(100, 100), IndexedSeq(1000, 1000), IndexedSeq(20, 20, 20), IndexedSeq(50, 50, 50))
  } new PerfExp[Boolean](s"$title, ${dims.mkString("x")}") {
    // Sets up the experiment from given parameters
    val lattice = title match {
      case "Boolean" => Lattice.boolean(false)(dims)
      case "Generic" => Lattice[Boolean](false)(dims)
    }
    val rule = Next42Boolean(neighbourhood)

    // Runs the experiment
    run { rule.apply(lattice) } { edit(f, true, false) }
  }
}
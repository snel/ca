/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca.tests.performance

/* Profiling utilities *******************************************************/

/** @define apply0 Creates a new TickingTimer for the code block that follows
  * @define apply1 Returns the elapsed time of running the block of code
  */
abstract class TimedObject {
}

/** Companion for TickingTimer */
object Timed extends TimedObject {
  /** $apply0. */
  def apply[A](block: (Timer) => A) = block(new TickingTimer())

  /** $apply1. */
  def apply(block: => Any): Long = {
    val t0 = System.currentTimeMillis()
    block
    System.currentTimeMillis() - t0
  }
}

/** Companion for PrintTimed */
object PrintTimed extends TimedObject {
  /** $apply1 and prints the result. */
  def apply[A](block: => A) = {
    val result = Timed(block)
    println(s"time elapsed = $result ms")
    result
  }
}

/** An immutable timer. */
abstract class Timer { def elapsedTime: Long }

/** A stopped Timer.
  *
  * Returns the length of time since it started until it stopped.
  */
case class StoppedTimer(override val elapsedTime: Long) extends Timer

/** A Timer that is ticking.
  *
  * Returns the length of time since it started.
  */
case class TickingTimer(startTime: Long = System.currentTimeMillis()) extends Timer {
  override def elapsedTime = System.currentTimeMillis() - startTime
  def stopped = new StoppedTimer(elapsedTime)
}
/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca
package tests
package performance

import java.util.Date

/** Displays performance statistics comparing different neighbourhood types.
  *
  * TODO: would be nice to save/load results and compare against previous runs
  * to provide regression testing.
  */
object NeighbourhoodTests extends App {
  println(getClass.getSimpleName)
  println(new Date())

  for {
    n <- Seq("Moore", "VonNeumann")
    r <- Seq(2)
    wrapped <- Seq(false, true)
    dims <- Seq(IndexedSeq(100, 100), IndexedSeq(1000, 1000), IndexedSeq(20, 20, 20), IndexedSeq(50, 50, 50))
  } new PerfExp[Int](s"$n, wrapped=$wrapped, ${dims.mkString("x")}") {
    // Sets up the experiment from given parameters
    val lattice = Lattice[Int](0)(dims)
    val neighbourhood = n match {
      case "Moore" => Moore(r, wrapped)
      case "VonNeumann" => VonNeumann(r, wrapped)
    }
    val rule = Next42(neighbourhood)

    // Runs the experiment
    run { rule.apply(lattice) } { edit(.5, 1, 0) }
  }
}

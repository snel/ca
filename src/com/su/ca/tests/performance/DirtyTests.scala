/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca
package tests
package performance

import java.util.Date

/** Displays performance statistics comparing BufferedLattices with and
  * without the Dirty trait.
  *
  * @todo need to update the Dirty trait to only set cells dirty if they actually changed
  */
object DirtyExperiments extends App {
  println(getClass.getSimpleName)
  println(new Date())

  for {
    dims <- Seq(IndexedSeq(50, 50, 50))
    f <- Seq(0, .05, .5, .95, 1)
    title <- Seq("clean", "dirty")
  } new PerfExp[Int](s"$title, ${f * 100}% of states changed, ${dims.mkString("x")}") {
    def edit(f: Double, value: Int) = {
      val p = 1 - f
      lattice.cells foreach { x =>
        if (Math.random() >= p) lattice(x) = value
      }
    }

    val lattice = Lattice[Int](0, dirty = (title == "dirty"))(dims)
    run { lattice.apply() } { edit(f, 1) }
  }
}
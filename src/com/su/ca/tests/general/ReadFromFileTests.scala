/*
 * CA project
 * @author D. D. Malan
 */

package com
package su
package ca
package tests
package general

import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.Before
import com.su.ca._

/** Tests reading grids from file input.
  */
class ReadFromFileTests {

  //Tests if basic file is read in correctly
  @Test def readBlinkerTest() {
    val expected = "0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
    val lattice = Lattice.boolFromFile("examples/blinker.txt")
    assertEquals("Blinker file not read correctly", lattice.toString(), expected)
  }

  //Tests if basic file is read in correctly
  @Test def readMediumTest() {
    val expected = "0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0"
    val lattice = Lattice.boolFromFile("examples/medium.txt")
    assertEquals("Medium file not read in correctly", lattice.toString(), expected)

  }

  //Tests if an exception is thrown if the file has a column that doesn't have enough entries
  @Test def readwrongsize() {
    try {
      val lattice = Lattice.boolFromFile("examples/wrongsize/txt")
      fail()
    } catch {
      case _: Exception => // Expected, so continue
    }
  }

  //Tests if an exception is thrown if the file has extra entries extending the columns
  @Test def readwrongsize2() {
    try {
      val lattice = Lattice.boolFromFile("examples/wrongsize2/txt")
      fail()
    } catch {
      case _: Exception => // Expected, so continue
    }
  }

  //Tests if an exception is thrown if the file has more rows than described in the dimensions
  @Test def readwrongsize3() {
    try {
      val lattice = Lattice.boolFromFile("examples/wrongsize3/txt")
      fail()
    } catch {
      case _: Exception => // Expected, so continue
    }
  }

  //Tests if an exception is thrown if the file has more columns than described in the dimensions
  @Test def readwrongsize4() {
    try {
      val lattice = Lattice.boolFromFile("examples/wrongsize4/txt")
      fail()
    } catch {
      case _: Exception => // Expected, so continue
    }
  }

  //Tests if the correct characters are given within the input file
  @Test def readwrongASCII() {
    try {
      val lattice = Lattice.boolFromFile("examples/wrongASCII/txt")
      fail()
    } catch {
      case _: Exception => // Expected, so continue
    }
  }

  //Tests if the dimensions given by the file are in the correct format
  @Test def readwrongdimensions() {
    try {
      val lattice = Lattice.boolFromFile("examples/wrongdimensions/txt")
      fail()
    } catch {
      case _: Exception => // Expected, so continue
    }
  }

  //Tests that the lattice read in is of the same format regardless of the spacing between entries in the input file
  @Test def readspacetest() {
    val expected = "1 0 1 1 0 0 0 0 0"
    val lattice = Lattice.boolFromFile("examples/spacetest.txt")
    assertEquals("Space test file not read in correctly", expected, lattice.toString())
  }
}
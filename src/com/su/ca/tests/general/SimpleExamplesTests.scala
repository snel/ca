/*
 * CA project
 * 
 * @author D. D. Malan
 * @author S. E. A. Nel
 */

package com
package su
package ca
package tests
package general

import org.junit.Test
import org.junit.Assert._
import java.io.File

/** Tests if the program can correctly implement simple CA examples.
  */
class SimpleExamplesTests {
  @Test def booleanLatticeTest() {
    val expected = "0 1 0 0 0 0 0 0 0"
    val lattice = Lattice.boolean()(IndexedSeq(3, 3))
    val x = Seq(0, 1)
    lattice(x) = true
    lattice.apply()
    assertEquals("Turn second cell in boolean lattice to true not successful", expected, lattice.toString)
  }

  //Tests if basic boolean file is tabulated correctly
  @Test def blinkerTestToTabulate() {
    import Lattice._

    val expected = "0 0 0 0 0 0\n0 0 0 1 0 0\n0 0 0 1 0 0\n0 0 0 1 0 0\n0 0 0 0 0 0\n0 0 0 0 0 0"
    val lattice = Lattice.boolFromFile("examples/blinker.txt")
    assertEquals("Blinker file not tabulated correctly", lattice.tabulate, expected)
  }

  @Test def make42Test() {
    val expected = "42 0 0 0 0 0"
    val lattice = Lattice[Int](0)(IndexedSeq(1, 2, 3))
    val x = Seq(0, 0, 0)
    lattice(x) = 42
    lattice.apply()
    assertEquals("Turn first cell in simple lattice to 42 not successful", expected, lattice.toString)
  }

  @Test def stringLatticeTest() {
    val expected = "bar foobar bar bar bar bar bar bar bar"
    val lattice = Lattice[String]("bar")(IndexedSeq(3, 3))
    val x = Seq(0, 1)
    lattice(x) = "foobar"
    lattice.apply()
    assertEquals("Turn second cell in simple string lattice to \"foobar\" not successful", expected, lattice.toString)
  }
  //Tests if basic string example is tabulated correctly
  @Test def stringTestToTabulate() {
    val expected = "   bar foobar    bar\n   bar    bar    bar\n   bar    bar    bar"
    val lattice = Lattice[String]("bar")(IndexedSeq(3, 3))
    val x = Seq(0, 1)
    lattice(x) = "foobar"
    lattice.apply()
    assertEquals("foobar not tabulated correctly", expected, lattice.tabulate(6))
  }

  @Test def wrappedMooreTest() {
    val expected = "42 42 42 42 0 0 0 42 42 1 42 42 0 0 0 42 42 42 42 42 0 0 0 42 42 42 42 42 0 0 0 42 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 42 42 42 42 0 0 0 42"
    val lattice = Lattice[Int](0)(IndexedSeq(8, 8))
    val x = Seq(1, 1)
    lattice(x) = 1
    lattice.apply()
    Next42(Moore(2, periodic = true)).apply(lattice)
    lattice.apply()
    assertEquals("Wrapped Moore example not successful", expected, lattice.toString)
  }

  @Test def wrappedVonNeumannTest() {
    val expected = "42 42 42 42 0 0 0 42 42 1 42 42 42 0 42 42 42 42 42 42 0 0 0 42 42 42 42 42 0 0 0 42 0 42 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 42 0 0 0 0 0 0 42 42 42 42 0 0 0 42"
    val lattice = Lattice[Int](0, dirty = true)(IndexedSeq(8, 8))
    val x = Seq(1, 1)
    lattice(x) = 1
    lattice.apply()
    Next42(VonNeumann(3, periodic = true)).apply(lattice)
    lattice.apply()
    assertEquals("Wrapped Von Neumann example not successful", expected, lattice.toString)
  }
}

class Next42(neighbourhood: Neighbourhood) extends Rule[Int] {
  /** Sets all states to 42, if any positive values are in the neighbourhood. */
  override def apply(lattice: BufferedLattice[Int]) = {
    lattice.dividedCells foreach { // this makes the rule work concurrently
      _ foreach { x =>
        if (pred(x, lattice)) lattice(x) = 42
      }
    }
  }

  def pred(x: Seq[Int], lattice: BufferedLattice[Int]) =
    neighbourhood(x, lattice).exists { y => x != y && lattice(y) > 0 }
}

object Next42 {
  def apply(neighbourhood: Neighbourhood) = new Next42(neighbourhood)
}

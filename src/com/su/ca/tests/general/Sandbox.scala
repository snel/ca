package com.su.ca.tests.general

import com.su.ca.tests.performance.Next42Boolean
import com.su.ca.gui.BooleanGrid
import com.su.ca.gui.GUI
import com.su.ca.gui.Grid
import java.awt.Color
import com.su.ca.Lattice
import com.su.ca.VonNeumann
import com.su.ca.Rule
import com.su.ca.ConwayBoolean
import com.su.ca.Moore
import scala.reflect.io.File
import scala.io.Source
import com.su.ca.gui.IntGrid

object Sandbox extends App {
  def A() {
    val expected = "0 0 0 0 0 0 0 42 0 1 0 0 0 0 42 42 0 0 0 0 0 0 0 42 0 0 0 0 0 0 0 42 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 42 0 0 0 0 0 0 42 42 42 42 0 0 0 42"
    expected.split(" ").sliding(8, 8).foreach { x => x.foreach { x => print(s"$x ") }; println() }
  }

  def B() {
    val f = "8 8\n" +
      "42 42 42 42 0 0 0 42\n" +
      "42 1 42 42 42 0 42 42 \n" +
      "42 42 42 42 0 0 0 42 \n" +
      "42 42 42 42 0 0 0 42 \n" +
      "0 42 0 0 0 0 0 0 \n" +
      "0 0 0 0 0 0 0 0 \n" +
      "0 42 0 0 0 0 0 0\n" +
      "42 42 42 42 0 0 0 42"
    val lattice = Lattice.intFromSource(Source.fromString(f))
    //    val x = Seq(1, 1)
    //    lattice(x) = 1
    //    lattice.apply()
    val rule = Next42(VonNeumann(3, periodic = true))

    GUI.main(Array.empty)
    GUI.fromSwingThread {
      GUI.updateGridPanel(IntGrid(lattice, rule))
      GUI.gridResized()
    }
  }

  def C() {
    val lattice = Lattice.apply(0)(IndexedSeq(8, 8))
    val x = Seq(1, 1)
    lattice(x) = 1
    lattice.apply()
    val rule = Next42(VonNeumann(3, periodic = true))

    GUI.main(Array.empty)
    GUI.fromSwingThread {
      GUI.updateGridPanel(IntGrid(lattice, rule))
      GUI.gridResized()
    }
  }
  
  def D() {
    val f = "8 8\n" +
      "42 42 42 42 0 0 0 42\n" +
      "42 1 42 42 42 0 42 42 \n" +
      "42 42 42 42 0 0 0 42 \n" +
      "42 42 42 42 0 0 0 42 \n" +
      "0 42 0 0 0 0 0 0 \n" +
      "0 0 0 0 0 0 0 0 \n" +
      "0 42 0 0 0 0 0 0\n" +
      "42 42 42 42 0 0 0 42"
    val lattice = Lattice.intFromSource(Source.fromString(f))
    println(lattice.toString)
  }

  C()
}
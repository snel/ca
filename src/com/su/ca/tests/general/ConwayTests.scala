/*
 * CA project
 * @author D. D. Malan
 */

package com
package su
package ca
package tests
package general

import org.junit.Test
import org.junit.Assert._

/** Tests if the program can correctly implement Conway's Game of Life.
  */
class ConwayTests {
  @Test def blinkerFromFileTest() {
    val expected = "0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
    val lattice = Lattice.boolFromFile("examples/blinker.txt")
    ConwayBoolean(Moore(1, periodic = false)).apply(lattice)
    lattice.apply()
    assertEquals("Conway's Game of Life not applied correctly to blinker example read from file", expected, lattice.toString)
  }

  @Test def blinkerFromSeqTest() {
    val expected = "0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
    val lattice = Lattice[Int](0)(IndexedSeq(6, 6))
    lattice(Seq(1, 3)) = 1
    lattice(Seq(2, 3)) = 1
    lattice(Seq(3, 3)) = 1
    lattice.apply()
    Conway(Moore(1, periodic = false)).apply(lattice)
    lattice.apply()
    assertEquals("Conway's Game of Life not applied correctly to blinker example created from sequence", expected, lattice.toString)
  }

  // FIXME medium_pulsar.txt missing from repo. 
  //  @Test def pulsarTest() {
  //    val expected = "0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 1 1 0 1 1 0 0 1 1 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 0 0 0 0 0 1 1 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 0 0 0 0 0 0 1 0 1 0 1 0 1 0 1 0 1 0 0 1 1 1 0 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0"
  //    val lattice = Lattice.boolFromFile("examples/medium_pulsar.txt")
  //    ConwayBoolean(Moore(1, periodic = false)).apply(lattice)
  //    lattice.apply()
  //    assertEquals("Conway's Game of Life not applied correctly to pulsar example", expected, lattice.toString)
  //  }

  @Test def toadTest() {
    val expected = "0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
    val lattice = Lattice.boolFromFile("examples/toad.txt")
    ConwayBoolean(Moore(1, periodic = false)).apply(lattice)
    lattice.apply()
    assertEquals("Conway's Game of Life not applied correctly to blinker example 2", expected, lattice.toString)
  }
}
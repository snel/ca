/*
 * CA project
 * @author S. E. A. Nel
 */

package com.su.ca

import scala.reflect.ClassTag

/** Multi-dimensional array.
  *
  * @note Values can be stored as simple integers/floats or whatever,
  * without any space overhead or boxing/unboxing! Neither is there any space
  * overhead for additional dimensions.
  *
  * @see Source: [[http://stackoverflow.com/questions/30623062/6-or-more-dimensional-arrays-in-scala?rq=1]]
  */
class MultiArray[A](val array: scala.collection.mutable.IndexedSeq[A], val dims: IndexedSeq[Int]) {
  def this(dims: IndexedSeq[Int])(implicit classtag: ClassTag[A]) = this(new Array[A](dims.product), dims)

  // Checking this here avoids having to check repeatedly during lookups
  require(dims.length > 0, "array cannot have 0 dimensions")
  require(array.length == dims.product)

  /** Gets the value at the specified coordinates. */
  def apply(index: Seq[Int]): A = array(arrayIndex(index))

  /** Optionally gets the value at the specified coordinates. */
  def get(index: Seq[Int]): Option[A] = {
    val k = arrayIndex(index)
    if (k >= 0 && k < array.length) Some(array(k)) else None
  }

  /** Updates the value at the specified coordinates. */
  def update(index: Seq[Int], value: A): Unit = array(arrayIndex(index)) = value

  /** Returns the index in the array corresponding to the given coordinates.
    * 
    * This assumes that index.length == dims.length.
    */
  protected def arrayIndex(index: Seq[Int]): Int = {
    var i = index.length - 1
    var k = index(i)
    var n = 1

    // This may look strange, since we count backwards, but this is necessary
    // to ensure the first coordinate is within bounds.
    if (index(0) < 0 || index(0) >= dims(0)) return -1 // first index out of bounds

    while (i > 0) {
      if (index(i) < 0 || index(i) >= dims(i)) return -1 // index out of bounds

      n *= dims(i)
      k += n * index(i - 1)
      i -= 1
    }
    k
  }
}

/** Companion for MultiArray. */
object MultiArray {
  /** Returns a MultiArray with the specified dimensions filled with the given
    * value.
    */
  def fill[A](dims: IndexedSeq[Int])(value: A)(implicit classtag: ClassTag[A]) =
    new MultiArray[A](Array.fill(dims.product)(value), dims)

  /** Returns a boolean MultiArray with the specified dimensions filled with
    * the given value.
    *
    * This implementation is highly efficient and scalable in terms of memory
    * use. In particular, almost no memory is required to store sparse vectors
    * of any size.
    */
  def fillBoolean(dims: IndexedSeq[Int])(value: Boolean) =
    new MultiArray[Boolean](BitArray.fill(dims.product)(value), dims)
}

/** Adapter for `BitSet` that behaves like a mutable, indexed sequence. */
// Note that BitSet.size may be less than the length of the array.
class BitArray(val bits: scala.collection.mutable.BitSet, override val length: Int) extends scala.collection.mutable.IndexedSeq[Boolean] {
  def this(length: Int) = this(new scala.collection.mutable.BitSet(length), length) // not sure if initializing it to this length is a good idea

  override def update(idx: Int, elem: Boolean) = bits(idx) = elem
  override def apply(idx: Int) = bits(idx)
}

/** Companion for BitArray. */
object BitArray {
  /** Returns an empty BitArray. */
  lazy val empty = new BitArray(0)

  /** Returns a BitArray of length `n` filled with 0's. */
  def apply(n: Int) = new BitArray(n)

  /** Returns a BitArray of length `n` filled with the value specified. */
  def fill(n: Int)(value: Boolean) = {
    val b = apply(n)
    if (value) b.indices.foreach { b(_) = true }
    b
  }
}
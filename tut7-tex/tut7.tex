\documentclass{article}
\usepackage[a4paper, margin=1.2in]{geometry}

% Math packages
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{float}

% Illustration packages
%\usepackage{tikz}
%\usetikzlibrary{positioning,shapes,arrows,automata}

% Custom math definitions

\newcommand{\Set}[2]{\{#1\ :\ #2\}}
\newcommand{\Or}{\ |\ }

\begin{document}
\title{CS 711 tutorial 7}

\author{
  S. E. A. Nel, \texttt{17903262}
  \and
  R. C. du Toit, \texttt{17816467}
  \and
  D. D. Malan, \texttt{17524695}
  \and
  K.R. De Klerk, \texttt{17861004}
}



\date{16/04/2016}

\maketitle
\section{Introduction}
For this assignment we had to develop a framework to handle cellular automata.
The design had to be general enough to use in the coming group project, 
making good use of the appropriate design patterns and data structures.

In particular, for this tutorial, we had to implement Conway's Game of Life,
which operates within the context of a boolean-valued two-dimensional lattice.
Here, a cell neighbourhood consists of the 8 cells surrounding a given cell. 
However, our implementation is flexible enough to deal with higher-dimensional 
lattices as well, containing any data type and using any neighbourhood.

The program can read a text file of a certain format describing 
the seed, or starting state of the lattice. Alternatively, one can draw a
configuration from within the program.
The Game of Life rule is then applied to the lattice and displayed in an animated
GUI. The program makes use of multi-threading to ensure the interface 
remains responsive during operation.

\section{Design}
We chose to write the framework in Scala. See section \ref{sec:Scala} for a 
discussion about this decision.
Apart from the GUI and utility code, the framework consists of 3 main parts.

\begin{figure}[tbp]
\begin{verbatim}
val lattice = Lattice.boolFromFile("blinker.txt")
ConwayBoolean(Moore(1)).apply(lattice)
\end{verbatim}

\label{fig:applyRule}
\emph{Listing \ref{fig:applyRule}\: An example of how to use the framework. 
We reconstruct a lattice from a given file, pass a Moore neighbourhood of 
size 1 to Conway's Game of Life rule and apply it once to the lattice.
} \\
\end{figure}

\begin{figure}[tbp]
\begin{verbatim}
class ConwayBoolean(neighbourhood: Neighbourhood) extends Rule[Boolean] {
  override def apply(lattice: BufferedLattice[Boolean]) = {
    // The rule works in parallel over each axis. x is the current cell.
    lattice.dividedCells.foreach {
      _ foreach { x =>
        // s is the number of live neighbours.
        val s = neighbourhood(x, lattice).map { y => 
          if (x != y && lattice(y)) 1 else 0 
        }.sum
        lattice(x) = lattice(x) match {
          case false => s == 3
          case true => s == 2 || s == 3
        }
      }
    }
  }
}
\end{verbatim}

\label{fig:conway}
\begin{em}Listing \ref{fig:conway}\: Conway's rule for boolean lattices. As defined on Wikipedia:
\begin{enumerate}
\item Any live cell with fewer than two live neighbours dies, as if caused by under-population.
\item Any live cell with two or three live neighbours lives on to the next generation.
\item Any live cell with more than three live neighbours dies, as if by over-population.
\item Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
\end{enumerate}
\end{em}
\end{figure}

\subsection{Lattice}
The first part is the lattice data structure. It provides methods to view and
update values in the lattice, and provides a lazily-computed iterator over valid
cell coordinates. See section \ref{sec:Visitor} for details.

The \texttt{BufferedLattice} class consists of two one-dimensional, generic arrays. 
While applying a rule, the first array remains invariant, and the second array is updated.
Once all the cells have been evaluated, changes to the second array are
copied back to the first. This ensures that each update is independent of the others and
no race conditions can occur.
Of course this is slightly less efficient than simply swapping the arrays after each update,
but this design is more resilient in that client code does not have to ensure that all cells were 
updated before applying the changes.

Although the arrays one-dimensional, we provide methods for manipulating them as 
though they are $n$-dimensional.
The fact that the arrays are generic means that we can represent cell states with any data type. 

Integer-valued lattices require $8n$ bytes of memory  (4 bytes per cell, for both arrays).
We also included a specialized implementation that stores boolean states as bit vectors. 
This means that boolean-valued lattices only require approximately $8n$ bits. 
These requirements could be reduced even further using an encoding scheme such as 
run-length encoding, at some cost to computational performance.

\subsection{Neighbourhood}
The second part defines a neighbourhood of cells, specifically a
list of cell coordinates. We include implementations for Moore and Von Neumann
neighbourhoods of any size. We also include variations for toroidal (i.e. periodic) lattices
that enable neighbourhoods to wrap around lattice boundaries.
For $n$-dimensional lattices, Moore neighbourhoods generalize to hyper-cubes,
while Von Neumann neighbourhoods generalize to hyper-spheres. 
Our implementation is general enough to construct such neighbourhoods of any 
size, for lattices of any size or dimension, whether periodic or not.

It is trivial to define non-adjacent neighbourhoods. For example, suppose we represent a
society as a lattice. Each person can speak to his physically adjacent neighbours,
or call a friend over the phone. We could represent people in a person's phone book
as $k$ randomly selected cells from the lattice. Our design supports creating scenarios such as
this.

\subsection{Rule}
The third part defines a rule to apply to the cells of a lattice. A rule may
make use of a neighbourhood, as defined above.
Rules may operate in parallel or in series. A marked improvement is observed
for some configurations when operating in parallel (see Table \ref{fig:perf1}).

\section{Testing}
\section{JUnit tests}
JUnit tests were written to test the correctness of our framework. 
The three core classes have at least 77\% coverage.
It is difficult to write tests for the GUI, because of complex multi-threaded behaviour. 
However, much manual testing was performed to ensure correct behaviour.

A non-fatal error in the Java 7 library was observed. 
Clicking on arrow buttons to change the time delay setting throws an exception under certain
conditions. This is documented in the code (see the definition for \texttt{GUI}), 
but does not impact the rest of the program, nor does it occur when running JRE 8.

\subsection{Performance tests}

Performance tests were written that compared the impact of various parameters on performance.
Of interest are results comparing serial and parallel performance (see Table \ref{fig:perf1}).

\section{Appendix}

\subsection{Scala}\label{sec:Scala}
We chose to write the framework in Scala. This language runs on the JVM and 
thus is compatible with Java code. Performance characteristics are 
similar to Java. The syntax is also very similar to Java, 
except that it expresses many common patterns more concisely. This enables 
developers to focus more on developing program logic and not on reading, 
writing and debugging boiler plate code. 
Here are some notable features of Scala:
\begin{enumerate}
  \item
  Generic programming in Scala supports both simple and complex data types. 
  In Java, a generic array of integers is necessarily an array of 
  \texttt{Integer} objects. This implies a memory overhead of 4 bytes per 
  entry, or twice as much as a simple \texttt{int} array.
  In Scala, there is no memory overhead for this. This means that we can run
  larger simulations while keeping the design simple.
  
  \item
  Scala is a hybrid functional and declarative object-oriented programming
  language. Being able to write declaratively gives us the freedom to write
  efficient low-level code where it matters. 
  But we can also leverage the best of both object-orientation and 
  functional programming idioms to facilitate separation of concern and 
  simplify code.
  Note that although Java 8 provides more support for functional programming 
  than previous versions, it remains verbose and is not as comprehensive as in 
  Scala.
  
  \item
  Scala facilitates lazy computation. This means we don't have to write complex 
  code to achieve lazy computation, we can use built-in language features instead
  to help write efficient algorithms.
  
  \item
  Scala simplifies common parallelization tasks.
  To write concurrent code in Java often requires relatively low-level 
  synchronization logic. There may be libraries that
  help with this in Java, but in Scala, concurrent operations on a list can be 
  achieved simply by writing \texttt{.par}.
\end{enumerate}

\subsection{Iterator vs. visitor pattern}\label{sec:Visitor}
We let rules iterate over cell coordinates, instead of letting the lattice
accept a visitor function, because this provides better separation of concern.
The client is free to decide which cells to visit, in which order,
and how to parallelize the operation.
The optimal iteration strategy depends on the application.
For example, when rendering the lattice contents, we need to iterate
in series.
When applying a simple rule and the lattice is sufficiently large, dividing
the lattice into slices which are concurrently operated on provides better performance.
If the lattice is sufficiently small, but great variation exists in the time taken
to compute a rule for each cell, better performance may be achieved by
a pipeline algorithm.
For these reasons, supplying a list is a better choice than accepting a visitor.

\subsection{Remarks on performance}
Table \ref{fig:perf1} displays some performance statistics.
Lattice sizes were chosen such that they all contain the same number
of cells. The same rule was applied in all cases: each cell is set to true 
regardless of its previous value.

To parallelize the operation we let separate threads compute each slice from 
the first dimension. 
Thus for the one-dimensional lattice, each cell is handed to a separate thread. 
We observe better performance in serial, in this case. This is likely because
of the overhead of starting a new thread for each cell and poor caching 
behaviour.
A marked improvement is observed when running in parallel
for higher-dimensional lattices.

Apart from the delays observed for parallel computation with the one-dimensional
lattice, in general, performance decreases as the number of dimensions increases. 
This is to be expected, because of the increased complexity of 
representing points in the space. Performance would of course
decrease exponentially if the rule used a Moore or Von Neumann neighbourhood. 
This is because the number of neighbours would decrease exponentially as the 
number of dimensions increases.

We conclude that the optimal parallelisation strategy is highly dependent on the
application and that allowing client code to specify the strategy was a
prudent design decision.
  
\begin{figure}[h]
\begin{center}
\begin{tabular}{lcccc}
 & \multicolumn{2}{c} {Serial} & \multicolumn{2}{c} {Parallel} \\
Lattice size & time (ms) & $\sigma$ (ms) & time (ms) & $\sigma$ (ms) \\
\hline
16777216                    & 1428 & 9 & 18115 & 6354 \\
$4096 \times 4096$          & 1967 & 4 & 491 & 28 \\
$256 \times 256 \times 256$ & 2834 & 8 & 660 & 21
\end{tabular}
\end{center}

 \label{fig:perf1}
  \begin{em}
  Table \ref{fig:perf1}\: Serial vs parallel performance.
  Shown are mean execution times and standard deviation over 10 samples
  for boolean lattices.
  
\end{em}
\end{figure}

\end{document}